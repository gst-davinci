/*
 * ASFParserInternal.h
 *
 * Header file for data Types in ASF Demuxer/Parser plugin 
 * for Gstreamer implementation on DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef _ASF_PARSER_INTERNAL_H_
#define _ASF_PARSER_INTERNAL_H_

#include <dataTypes.h>
#include <ASFParserError.h>


// IFNDEF GET_BYTE defined in drmbytemanip.h
#ifndef ASFDRMDEFINE
#define GET_BYTE(pb,ib)             (pb)[(ib)]
#define PUT_BYTE(pb,ib,b)           (pb)[(ib)]=(b)
#endif // ASFDRMDEFINE 

#define LITTLE_ENDIAN_TARGET    1

#define ASF_BYT_CopyBytes(to,tooffset,from,fromoffset,count)    \
        _Memcpy(&((to)[(tooffset)]),&((from)[(fromoffset)]),(count))
            
#define ASF_BYT_MoveBytes(to,tooffset,from,fromoffset,count)    \
        _ASFMemmove(&((to)[(tooffset)]),&((from)[(fromoffset)]),(count))
            
#define ASF_BYT_SetBytes(pb,ib,cb,b)                            \
            _ASFMemset(&((pb)[(ib)]),b,cb)
            
#define ASF_BYT_CompareBytes(pbA,ibA,pbB,ibB,cb)                \
            _ASFMemcmp(&((pbA)[(ibA)]),&((pbB)[(ibB)]),(cb))
            

#define _ASFIsEqualGUID( rguid1, rguid2 )                       \
     ( ! ASF_BYT_CompareBytes( rguid1, 0,rguid2,0, sizeof( GUID ) ) )

#if LITTLE_ENDIAN_TARGET == 1
#define BYTES_TO_DWORDOFFSET            BYTE2LITTLEENDIANOFFSET
#define BYTES_TO_WORDOFFSET             BYTE2LITTLEENDIANWORDOFFSET
#define DWORD_TO_BYTESOFFSET            DWORD2LITTLEENDIANOFFSET

#define BYTES_TO_QWORDOFFSET( qword, byte, i )          \
                    BYTES2QWORD_LEOFFSET( qword, byte, i )
                     
#define WORD_TO_BYTESOFFSET( byte, word, i )            \
                    WORD2BYTES_LEOFFSET( byte, word, i )
                    
#define QWORD_TO_BYTESOFFSET( byte, qword, i )          \
                    QWORD2BYTES_LEOFFSET( byte, qword, i )

#else /* for BIG_ENDIAN_TARGET */
#define BYTES_TO_DWORDOFFSET            BYTE2BIGENDIANOFFSET
#define BYTES_TO_WORDOFFSET             BYTE2BIGENDIANWORDOFFSET
#define DWORD_TO_BYTESOFFSET            DWORD2BIGENDIANOFFSET

#define BYTES_TO_QWORDOFFSET( qword, byte, i )          \
                    BYTES2QWORD_BEOFFSET( qword, byte, i )
                    
#define WORD_TO_BYTESOFFSET( byte, word, i )            \
                    WORD2BYTES_BEOFFSET( byte, word, i )
                    
#define QWORD_TO_BYTESOFFSET( byte, qword, i )          \
                    QWORD2BYTES_BEOFFSET( byte, qword, i )
#endif /* End TARGET_LITTLE_ENDIAN */


#define WORD2BYTES_BEOFFSET( byte, word, i )            \
            {FIX_ENDIAN_WORD((word));                   \
            ASF_BYT_CopyBytes(                          \
            (byte),(i),( BYTE *)&(word),0,sizeof( UINT16));      \
            FIX_ENDIAN_WORD((word));}
            
#define QWORD2BYTES_BEOFFSET( byte, qword, i )              \
            {FIX_ENDIAN_QWORD((qword));                     \
            ASF_BYT_CopyBytes(                              \
            (byte),(i),( BYTE *)&(qword), 0, sizeof( UINT64));    \
            FIX_ENDIAN_QWORD((qword));}
                  
#define BYTES2QWORD_BEOFFSET( qword, byte, i )                      \
            {ASF_BYT_CopyBytes(                                     \
            ( BYTE *)&(qword),0,(byte),(i),sizeof( UINT64));    \
            FIX_ENDIAN_QWORD((qword));}

#define WORD2BYTES_LEOFFSET( byte, word, i )        \
            ASF_BYT_CopyBytes(                      \
                (byte),(i),( BYTE *)&(word),0,sizeof( UINT16));
                
#define QWORD2BYTES_LEOFFSET( byte, qword, i )      \
            ASF_BYT_CopyBytes(                      \
                (byte),(i),( BYTE *)&(qword),0,sizeof( UINT64));
                
#define BYTES2QWORD_LEOFFSET( qword, byte, i)       \
            ASF_BYT_CopyBytes(                      \
                ( BYTE *)&(qword),0,(byte),(i),sizeof( UINT64));


#define BYTE2LITTLEENDIANOFFSET(dword, byte,i)      \
            ASF_BYT_CopyBytes(                      \
                ( BYTE *)&(dword),0,(byte),(i),sizeof( UINT32));
                
#define BYTE2LITTLEENDIANWORDOFFSET(word, byte,i)   \
            ASF_BYT_CopyBytes(                      \
                ( BYTE *)&(word),0,(byte),(i),sizeof( UINT16));
                
#define DWORD2LITTLEENDIANOFFSET( byte, dword,i )   \
            ASF_BYT_CopyBytes(                      \
                (byte),(i),( BYTE *)&(dword),0,sizeof( UINT32));
                
#define BYTE2BIGENDIANWORDOFFSET(word, byte,i)      \
            {ASF_BYT_CopyBytes(                     \
                ( BYTE *)&(word),0,(byte),i,sizeof( UINT16));    \
                FIX_ENDIAN_WORD((word));}
                
#define BYTE2BIGENDIANOFFSET(dword, byte,i)         \
            {ASF_BYT_CopyBytes(                     \
                ( BYTE *)&(dword),0,(byte),i,sizeof( UINT32));  \
                FIX_ENDIAN_DWORD((dword));}
                
#define DWORD2BIGENDIANOFFSET(byte, dword,i)        \
            {FIX_ENDIAN_DWORD((dword));             \
             ASF_BYT_CopyBytes(                     \
                (byte),(i),( BYTE *)&(dword),0,sizeof( UINT32)); \
                FIX_ENDIAN_DWORD((dword)); }



#define _loadBYTEOffset( b, p, i )       b = GET_BYTE(p, i);    \
                                            i += sizeof ( BYTE);
                                        
#define _saveBYTEOffset( p, b, i )       PUT_BYTE(p, i, b);     \
                                            i += sizeof ( BYTE);
                                            
#define _loadWORDOffset( w, p, i )       BYTES_TO_WORDOFFSET(   \
                                            (w),(p), (i) );     \
                                            (i) += sizeof(  UINT16 )
                                        
#define _saveWORDOffset( p, w, i )       WORD_TO_BYTESOFFSET(   \
                                            (p), (w), (i) );    \
                                            (i) += sizeof(  UINT16 )
                                        
#define _saveWORDOffset( p, w, i )       WORD_TO_BYTESOFFSET(   \
                                            (p), (w), (i) );    \
                                            (i) += sizeof(  UINT16 )
                                        
#define _loadDWORDOffset( dw, p, i )     BYTES_TO_DWORDOFFSET(  \
                                            (dw), (p), (i) );   \
                                            (i) += sizeof(  UINT32 )
                                        
#define _saveDWORDOffset( p, dw, i )     DWORD_TO_BYTESOFFSET(  \
                                            (p), (dw), (i) );   \
                                            (i) += sizeof(  UINT32 )
                                            
#define _loadQWORDOffset( qw, p, i )     BYTES_TO_QWORDOFFSET(  \
                                            (qw), (p), (i) );   \
                                            (i) += sizeof(  UINT64 )
                                        
#define _saveQWORDOffset( p, qw, i )     QWORD_TO_BYTESOFFSET(  \
                                            (p), (qw),(i) );    \
                                            (i) += sizeof(  UINT64 )
                                         

#define _loadASFGUID( g, p, i )                             \
        {                                                   \
            _loadDWORDOffset( (g).Data1, p, i );                  \
            _loadWORDOffset( (g).Data2, p, i );                   \
            _loadWORDOffset( (g).Data3, p, i );             \
            PUT_BYTE((g).Data4, 0, GET_BYTE(p, i));         \
            PUT_BYTE((g).Data4, 1, GET_BYTE(p, (i + 1)));   \
            PUT_BYTE((g).Data4, 2, GET_BYTE(p, (i + 2)));   \
            PUT_BYTE((g).Data4, 3, GET_BYTE(p, (i + 3)));   \
            PUT_BYTE((g).Data4, 4, GET_BYTE(p, (i + 4)));   \
            PUT_BYTE((g).Data4, 5, GET_BYTE(p, (i + 5)));   \
            PUT_BYTE((g).Data4, 6, GET_BYTE(p, (i + 6)));   \
            PUT_BYTE((g).Data4, 7, GET_BYTE(p, (i + 7)));   \
            i += 8;                                         \
        }



#define ASF_INT64Add(a, b)      ( (a) + (b) )
#define ASF_INT64Sub(a, b)      ( (a) - (b) )
#define ASF_INT64Mul(a, b)      ( (a) * (b) )
#define ASF_INT64Div(a, b)      ( (a) / (b) )
#define ASF_INT64Mod(a, b)      ( (a) % (b) )
#define ASF_INT64And(a, b)      ( (a) & (b) )
#define ASF_INT64ShR(a, b)      ( (a) >> (b) )
#define ASF_INT64ShL(a, b)      ( (a) << (b) )
#define ASF_INT64Eql(a, b)      ( (a) == (b) )
#define ASF_INT64Les(a, b)      ( (a) < (b) )
#define ASF_INT64(b)            ( ( INT64) (b) )
#define ASF_INT64Asgn(a, b)     ((( INT64)(a)<<32) & (b))
#define ASF_UINT32INT64(b)      (( INT64)(b))

#define ASF_INT64ToUINT32(b)    (( UINT32)(b))


#define ASF_UINT64Add(a, b)     ( (a) + (b) )
#define ASF_UINT64Sub(a, b)     ( (a) - (b) )
#define ASF_UINT64Mul(a, b)     ( (a) * (b) )
#define ASF_UINT64Div(a, b)     ( (a) / (b) )
#define ASF_UINT64Mod(a, b)     ( (a) % (b) )
#define ASF_UINT64And(a, b)     ( (a) & (b) )
#define ASF_UINT64ShR(a, b)     ( (a) >> (b) )
#define ASF_UINT64ShL(a, b)     ( (a) << (b) )
#define ASF_UINT64Eql(a, b)     ( (a) == (b) )
#define ASF_UINT64Les(a, b)     ( (a) < (b) )
#define ASF_UINT64(b)           ( ( UINT64) (b) )


#define SIZEOF_U8                                       1

#define ASF_MIN_OBJ_SIZE                                24
#define ASF_DATA_OBJECT_SIZE                            50

#define ASF_RIGHT_PLAYBACK_MASK                         0xFFFE
#define ASF_RIGHT_COLLABORATIVE_PLAY_MASK               0xFFFD
#define ASF_RIGHT_COPY_CD_MASK                          0xFFFB
#define ASF_RIGHT_COPY_MASK                             0xFFF7
#define ASF_RIGHT_COPY_TO_SDMI_DEVICE_MASK              0xFFF3
#define ASF_RIGHT_COPY_TO_NON_SDMI_DEVICE_MASK          0xFFEF
#define ASF_RIGHT_BACKUP_MASK                           0xFFDF
#define ASF_RIGHT_PLAYLISTBURN_MASK                     0xFFBF

typedef  VOID*                            ASFCONTENTHANDLE;

/*
 * Stream parsing state 
 */
typedef enum 
{
    P_NOT_VALID = 0,
    P_NEW_PACKET,
    P_MIDDLE_PACKET
} ASFPARSESTATE;


/*
 * Structure containing the offset of the ASF header objects
 */
typedef struct
{
    /* Content encryption object offset */
     INT32        lContentEncObjOff;
    
    /* Extended content encryption object offset */
     INT32        lExtContentEncObjOff;
    
    /* Content description object offset */
     INT32        lContentDescObjOff;
    
    /* Extended content description object offset */
     INT32        lExtContentDescObjOff; 
    
    /* Header extension object offset */
     INT32        lHeaderExtnObjOff;
    
    /* Codec List object offset */
     INT32        lCodecListObjOff;
    
    /* Language List object offset */
     INT32        lLangListObjOff;
    
    /* Metadata object offset */
     INT32        lMetadataObjOff;
    
    /* Stream bitrate properties object offset*/
     UINT32       lStreamBitrateObjOff;
    
    /* Stream properties1 object */
     UINT32       lStreamProp1ObjOff;
} ASFOBJSOFFSET;

/*
 * Structure contaning the ASF Data packet header information
 */
typedef struct 
{
     BOOL         fParityPacket;
     BOOL         fEccPresent;
     BOOL         fMultiPayloads;
     UINT32       dwParseOffset;
     BYTE        bECLen;
     BYTE        bPacketLenType;
     BYTE        bPadLenType;
     BYTE        bSequenceLenType;
     UINT32       dwPacketLenTypeOffset;
     BYTE        bOffsetBytes;
     BYTE        bOffsetLenType;
     BYTE        bPayLenType;
     BYTE        bPayBytes;
     UINT32       dwPacketLenOffset;
     UINT32       dwExplicitPacketLength; 
     UINT32       dwSequenceOffset;
     UINT32       dwSequenceNum; 
     UINT32       dwPadLenOffset;
     UINT32       dwPadding;
     UINT32       dwSCR;
     UINT32       dwPayLenTypeOffset;
     UINT32       cPayloads;
     UINT32       cPayloadsLeft;
     UINT16       wDuration;
} ASFPACKETPARSEINFO;

/*
 * Structure contaning the ASF payload header information
 */
typedef struct 
{
     UINT16       wPacketOffset;
     UINT16       wTotalSize;

     BYTE        bStreamId;
     BYTE        bObjectId;
     BYTE        bRepData;
     BYTE        bJunk;

     UINT32       dwObjectOffset;
     UINT32       dwObjectSize;
     UINT32       dwObjectPres;

     BOOL         fIsKeyFrame;
     BOOL         fIsCompressedPayload;
    /* 
     * Offset of this payload from the start of the data packet 
     */
     UINT32       dwPayloadOffset;
     UINT32       dwPayloadSize;
     UINT16       wTotalDataBytes;
     UINT32       dwDeltaPresTime;

     UINT16       wBytesRead;
     BYTE        bSubPayloadState;
     BYTE        bNextSubPayloadSize;
     UINT16       wSubpayloadLeft;
     UINT16       wSubCount;
} ASFPAYLOADPARSEINFO;

/*
 * Structure containing parsing information about active audio stream
 */
typedef struct 
{
    /*
     * Number of audio or video streams in the ASF file 
     */
     UINT32               cStreams;
    
    /*
     * Active  stream number 
     */
     BYTE                bActiveStreamId;
    
    /*
     * Decrypt the stream or not 
     */
     BOOL                 fDecryptStream;
    
    /*
     * stream seek offset
     */
     UINT32               dwStreamSeekPt;
       
    /*
     * Current data packet offset
     */
     UINT64               qwCurrentPacketOffset;
    
    /*
     * Next packet offset
     */
     UINT64               qwNextPacketOffset;
         
    /*
     * In the middle of a data packet
     */
    ASFPARSESTATE           ParseState;
    
    /*
     * Whether the seek point is set
     */
     BOOL                 fSeekPtSet;
    
    /*
     * Packet header information
     */
    ASFPACKETPARSEINFO      asfPacketInfo;
    
    /*
     * payload header information
     */ 
    ASFPAYLOADPARSEINFO     asfPayloadInfo; 
    
    /*
     * Payload header has been parsed
     */   
     BOOL                 fPayloadParsed; 
} STREAMPARSEINFO;


/* Prototype of the read callback function */
typedef  STATUS (*readCb)(ASFCONTENTHANDLE  asfHandle, 
                         BYTE **pbBuffer,  UINT32 fileOffset, 
                         UINT32 cBytes);
/* Prototype of the write callback function */                        
typedef  STATUS (*writeCb)(ASFCONTENTHANDLE asfHandle, 
                          BYTE *pbBuffer,  UINT32 fileOffset, 
                          UINT32 cBytes);



/* 
 * State of the media stream
 */
typedef struct
{
   
    BYTE     bStreamId;
   
    BOOL      fEncrypted; 
} STREAMSTATE;

#ifdef ASFDRMDEFINE
#include <drmcommon.h>
#include <drmutilities.h>
#include <drmcontextsizes.h>
#include <drmmanager.h>
#endif // ASFDRMDEFINE

typedef struct 
{
    /*
     * Pointer I/O read function
     */
    readCb                              read;
     
    /*
     * Pointer I/O write function
     */
    writeCb                             write;
    
    
    /*
     * Opaque handle to an asf file 
     */
    ASFCONTENTHANDLE                       hAsfContent;    
    
    /*
     * ASF Header size
     */
     UINT64                           qwHeaderSize;  
    
    /*
     * ASF Header extension size
     */
     UINT32                           dwHeaderExtnSize;
    
    /*
     * Number of header objects
     */
     UINT32                           cHeaderObjs;
     
    /*
     * Total number of data packets in the ASF file 
     */
     UINT64                           cDataPackets;
    
    /*
     * Size of the Data packet
     */
     UINT32                           dwPacketLength;
    
    /*
     * Size of the ASF file
     */
     UINT64                           qwFileSize;
         
    /*
     * Total Play time in milliseconds 
     */
     UINT64                           qwDuration;
    
    /*
     * Specifies amount of time to buffer the data before starting to
     * play
     */
     UINT64                           qwPreroll;
    
    /*
     * Byte offset of the first packet
     */
     UINT32                           dwFirstPacketOffset;
    
    /*
     * Current offset in the file 
     */
     INT32                            lCurrentFileOffset;
    
    /*
     * Last packet offset in the file
     */
     UINT32                           dwLastPacketOffset;
    
    /*
     * Offset of the next packet in the file to parse 
     */
     UINT32                           qwNextPacketOffset;
    
    /*
     * DRM protected content
     */
     BOOL                             fProtectedContent;
    
    /*
     * Structure containing the audio stream parsing information
     */
    STREAMPARSEINFO                     audioParseInfoObj;
    
    /*
     * Structure containing the video stream parsing information
     */
    STREAMPARSEINFO                     videoParseInfoObj;
    
    /*
     * Structure containing the offsets of the ASF objects 
     */
    ASFOBJSOFFSET                       asfObjsOffset;

#ifdef ASFDRMDEFINE

    /*
     * Decrypt context for a ASF content 
     */
    DRM_MANAGER_DECRYPT_CONTEXT        *pDecryptContext; 
    
    /*
     * DRM session context for an ASF content 
     */
    DRM_MANAGER_CONTEXT                *pManagerContext; 
#endif // ASFDRMDEFINE

} ASFPARSERPBCONTEXTINTERNAL;

#endif //_ASF_PARSER_INTERNAL_H_
