/*
 * RCA.h
 *
 * Header file for data Types in ASF Demuxer/Parser plugin 
 * for Gstreamer implementation on DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */


#ifndef _RCA_H_
#define _RCA_H_

#include <ASFParser.h>

#define RCA_SEQUENCE_HEADER_LEN 70
#define RCA_FRAME_HEADER_LEN 5

 STATUS RCAGenerateSequenceHeader(
			ASFPARSERPBCONTEXTHANDLE pContext,
			ASFAUDIOSTREAMINFO *pAudStreamInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);
 STATUS RCAGenerateFrameHeader(
			ASFPARSERPBCONTEXTHANDLE pContext,
			MEDIAOBJMETAINFO *pMediaObjMetaInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);

#endif //_RCA_H_
