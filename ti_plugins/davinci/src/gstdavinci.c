/*
 * gstadecoder.h
 *
 * Header File for Audio Decoder plugin for Gstreamer
 *
 * Copyright (C) 2008 Behind The Set, LLC
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstfbvideosink.h"
#include "gstadecoder.h"
#include "gstgdecoder.h"

static gboolean plugin_init(GstPlugin * plugin)
{

    if (!gst_element_register
            (plugin, "fbvideosink", GST_RANK_PRIMARY, GST_TYPE_FBVIDEOSINK))
        return FALSE;

    if (!gst_element_register
            (plugin, "adecoder", GST_RANK_PRIMARY, GST_TYPE_ADECODER))
        return FALSE;

    if (!gst_element_register
            (plugin, "gdecoder", GST_RANK_PRIMARY, GST_TYPE_GDECODER))
        return FALSE;


    return TRUE;
}


GST_PLUGIN_DEFINE(GST_VERSION_MAJOR, GST_VERSION_MINOR,
                  "davinci", "Plugins for Ti daVinci",
                  plugin_init, "0.1", GST_LICENSE_UNKNOWN,
                  "TI", "http://www.ti.com")

