/*
 * AVIParser.h
 *
 * Header file for data Types used in AVI Parser plugin
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef _AVI_PARSER_H_
#define _AVI_PARSER_H_

#include <dataTypes.h>
#include <AVIParserError.h>
#include <AVIParserInternal.h>


/*
 * Metadata supported by the AVI Parser
 */
/*
 * TITLE field.
 */
#define TITLE                               0xF001

/*
 * ALBUM field.
 */
#define ALBUM                               0xF002

/*
 * ARTIST field.
 */
#define ARTIST                              0xF003

/*
 * GENRE field.
 */
#define GENRE                               0xF004
        
/*
 * ALBUMARTIST field.
 */                             
#define ALBUMARTIST                         0xF005

/*
 * DESCRIPTION field.
 */
#define DESCRIPTION                         0xF006

/*
 * Rating field.
 */
#define RATING                              0xF007
            
/*
 * TOTALPLAYTIME field.
 */            
#define TOTALPLAYTIME                       0xF008

/*
 * PREROLL field.
 */
#define PREROLL                             0xF009
    
/*
 * AUDIO CODEC type field.
 */    
#define AUDIOCODEC                          0xF00A

/*
 * VIDEO CODEC type field.
 */
#define VIDEOCODEC                          0xF00B



typedef AVIPARSERPBCONTEXTINTERNAL          AVIPARSERPBCONTEXT;
typedef AVIPARSERPBCONTEXT*                 AVIPARSERPBCONTEXTHANDLE;


/*
 * Type of the video codec used to encode the video stream
 */ 
typedef enum 
{
    INVALID_VID = 0,
    WMV1_VID,
    WMV2_VID,
    WMV3_VID,
    UNKNOWN_VID
} VIDCODECTYPE;

#define COMPRESSIONID_WMV1  0x31564D57
#define COMPRESSIONID_WMV2  0x32564D57
#define COMPRESSIONID_WMV3  0x33564D57
#define COMPRESSIONID_MP43  0x3334504D
#define COMPRESSIONID_MP4S  0x5334504D
#define COMPRESSIONID_MS42  0x3234534D

#define COMPRESSIONID_mp43  0x3334706D
#define COMPRESSIONID_mp4s  0x7334706D
#define COMPRESSIONID_ms42  0x3234736D

/*
 * Create aliases for the codec types supported by the AVI Spec 01.20.30
 *  - See Chapter 11 of spec
 */
#define WMA_SERIES7_9           0x0161
#define WMA9_PRO                0x0162
#define WMA9_LSL                0x0163
#define GSM_AMR_FIXED           0x7A21
#define GSM_AMR_VAR             0x7A22

/* Album ID length */
#define WM_ALBUM_ID_LEN             28
/* Artist ID length */
#define WM_ARTIST_ID_LEN            30
/* Other ID length */
#define WM_OTHER_ID_LEN             18
/* Maximum ID length */
#define WM_MAX_ID_LEN               WM_ARTIST_ID_LEN
/* Maximum stream language ID */
#define MAX_STREAM_LANG_ID          24
/*
 * MPEG Video FourCC codes
 */
typedef enum
{
    FOURCC_UNKNOWN = 0,
    FOURCC_MP4S = 1,
    FOURCC_MS42 = 2,
    FOURCC_MP43 = 3,
    FOURCC_WMV1 = 4,
    FOURCC_WMV2 = 5,
    FOURCC_WMV3 = 6
} VIDEOFOURCCCODEC;


/*
 * Language of an AVI stream
 */
typedef enum
{
    /*
     * unknown language
     */
    UNKNOWN = 0,
    
    /*
     * English  US
     */
    EN_US,
    
    /*
     * Croatian
     */
    HR, 
    
    /*
     *  Hebrew 
     */
    HE, 
    
    /*
     * French
     */
    FR,
    
    /*
     * Japanese
     */
    JA 
} AVISTREAMLANG;


/*
 * Video encoding profile sued to encode the video stream
 */
typedef enum 
{
    VIDEOENCPROFILE_SIMPLE      = 0,
    VIDEOENCPROFILE_MAIN        = 4,
    VIDEOENCPROFILE_ADVANCED    = 12
} VIDEOENCPROFILE;

/*
 * Bitrate type of an AVI stream
 */
typedef enum 
{
    BITRATE_UNKNOWN             = 0,
    BITRATE_CONSTANT            = 1,
    BITRATE_VARIABLE            = 2
} AVISTREAMBITRATETYPE;

/*
 * Stream to seek
 */
typedef enum {
    SEEK_AUDIO = 0,
    SEEK_VIDEO = 1,
    SEEK_AUDIO_VIDEO = 2
} SEEKSTREAM;


/*
 * Type of a stream in an AVI file 
 */
typedef enum {
    AUDIO                       = 1,
    VIDEO                       = 2
} AVISTREAMTYPE;



/* Prototype of the read callback function */
typedef  STATUS (*readCallback)(AVICONTENTHANDLE AVIHandle, 
                         BYTE **pbBuffer,  UINT32 fileOffset, 
                         UINT32 cBytes);
/* Prototype of the write callback function */                        
typedef  STATUS (*writeCallback)(AVICONTENTHANDLE AVIHandle, 
                          BYTE *pbBuffer,  UINT32 fileOffset, 
                          UINT32 cBytes);

/*
 * Structure containing the minimum output protection levels 
 */
typedef struct 
{
     UINT16                      wCompressedDigitaVideo;
     UINT16                      wUnCompressedDigitalVideo;
     UINT16                      wAnalogAudio;
     UINT16                      wCompressedDigitalAudio;
     UINT16                      wUnCompressedDigitalAudio;
} MINOPPROTECTIONLVL;


/*
 * Structure to hold the information about a single metadata
 */
typedef struct 
{
    /*
     * ID of the metadata 
     */
	 UINT32                      dwTagID;
    
    /*
     * Buffer to hold the metadata
     */
	 UINT32                      *pBuffer;
    
    /*
     * length of the buffer
     */
	 UINT32                      cBuffer;
}METADATAOBJ;



/*
 * Structure containing the file operation callbacks
 */
typedef struct 
{
	/*
     * Pointer I/O read function
     */
    readCallback                read;

    /*
     * Pointer I/O write function
     */
    writeCallback               write;
} FILEOPS;

/*
 * Structure containing  the meta information about a media object
 */
typedef struct 
{
    /*
     * Media object number to which the payload belongs. A media 
     * object could be Frame in a video stream 
     */
     UINT32       dwMediaObjNum; 
    
    /*
     * Size of the media object to which this payload belongs to 
     */ 
     UINT32       dwMediaObjSize; 
                    
    /*
     * Media object presentation time 
     */                     
     UINT32       dwMediaObjPresTime; 
    
    /*
     *  Is it compressed object 
     */
     BOOL         fCompressed; 
    
    /*
     *  Is it key frame
     */
     BOOL         fKeyFrame; 
} MEDIAOBJMETAINFO;

// Structure containing wave format specific data
typedef struct {
	 UINT16	wFormatTag;
	 UINT16	cChannels;
	 UINT32	dwSamplesPerSec;
	 UINT32	dwAvgBytesPerSec;
	 UINT16	wBlockAlign;
	 UINT16	wBitsPerSample;
	 UINT16	cbSize;
}WAVEFORMATEX, *PWAVEFORMATEX;

// Structure containing WMA specific Data
typedef struct{
	WAVEFORMATEX		WaveFormatEx;
	 UINT32	dwSamplesPerBlock;
	 UINT16	wEncodeOptions;
	 UINT32	dwSuperBlockAlign;
}WMATYPESPECIFICDATA,*PWMATYPESPECIFICDATA;


/*
 * Structure containing the audio stream specific information 
 */
typedef struct
{
    /*
     * Total number of media objects 
     */
     UINT64       cMediaObjects; 
    
    /*
     *  Total play duration 
     */
     UINT64       qwPlayDuration; 
   
    /*
     * Size of the media object
     */
     UINT32       dwMediaObjSize; 
    
    /*
     * 16-byte Audio stream GUID 
     */
     GUID         audStreamType; 
    
    /*
     * Total length of the type specific data 
     */
     UINT32       dwTypeSpecDataLen; 
    
    /*
     * ID of the stream 
     */
     UINT16       wFlags; 
 
    WMATYPESPECIFICDATA	WMATypeSpecData;

} AVIAUDIOSTREAMINFO;

/*
 * Structure containing the video stream specific information
 */
typedef struct
{
    
     INT32                    lFrameHeight;
    
     INT32                    lFrameWidth;
    
    /*
     * leak rate R rate in bits per second
     */
     UINT32                   dwHrdRate; 
    
    /*
     * buffer size of leaky bucket B in milliseconds
     */
     UINT32                   dwHrdBuffSize; 
    
    /* 
     * MPEG Video FourCC code 
     */
    VIDEOFOURCCCODEC            videoFourCCCodec; 
    
    /*
     * Maximum Media object size
     */
     UINT32                   dwMaxMediaObjSize;
     
    /*
     * Video Encoding Profile 
     */
    VIDEOENCPROFILE             vidEncProfile; 
    
    /*
     * Frame rate- number of frames per sec (fps) 
     */
     UINT32                   dwScale;

    /*
     * Frame rate- number of frames per sec (fps) 
     */
     UINT32                   dwRate; 
    
    /*
     * Count of bits per pixel 
     */
     UINT16                   cBitPerPixel; 
    
    /*
     * Size of the image in bytes 
     */
     UINT32                   dwImageSize; 
    
    /*
     * Horizontal Pixels Per Meter
     */
     INT32                    cXPelsPerMeter; 
    
    /*
     * Vertical Pixels Per Meter
     */
     INT32                    cYPelsPerMeter; 
    
    /*
     * Colors used count
     */
     UINT32                   cClrUsed; 
    
    /*
     * Important colors count
     */
     UINT32                    cClrImportant; 
    
    /*
     * codec specific information appended to the
     * end of the BITMAPINFOHEADER structure 
     */
     BYTE         pCodecSpecData[4];
    
} AVIVIDEOSTREAMINFO;


/*
 * Minimum information about a stream in an AVI file
 */
typedef struct
{
     BYTE                bStreamId;
    
    /*
     * Type of an AVI stream
     */
    AVISTREAMTYPE           streamType;
    
    /* 
     * Length of the Stream Language ID in terms of 
     * WORDs (unsigned short) 
     */
     BYTE                bStreamLangIdLen;
    
    /*
     * Language type of the stream
     */
     UINT16               wStreamLangId[MAX_STREAM_LANG_ID]; 
    
     
    AVISTREAMLANG           streamLangType;
        
    /*
     * If the stream is of Variable bit rate or Constant bit rate 
     */
    AVISTREAMBITRATETYPE    streamBitrateType; 
    
    /*
     * Bit rate of the stream 
     */
     UINT32               streamBitrate;
    
    /*
     * Whether the stream is encrypted or not
     */
     UINT32               fEncryptedStream;
} MINSTREAMINFO;

typedef struct _avistreamheader {
	 UINT32  fcc;
	 UINT32  cb;
	 UINT32  fccType;
	 UINT32  fccHandler;
	 UINT32  dwFlags;
	 UINT16  wPriority;
	 UINT16  wLanguage;
	 UINT32  dwInitialFrames;
	 UINT32  dwScale;
	 UINT32  dwRate;
	 UINT32  dwStart;
	 UINT32  dwLength;
	 UINT32  dwSuggestedBufferSize;
	 UINT32  dwQuality;
	 UINT32  dwSampleSize;
	struct {
		 UINT32 left;
		 UINT32 top;
		 UINT32 right;
		 UINT32 bottom;
	}  rcFrame;
} AVISTREAMHEADER, *PAVISTREAMHEADER;

/***********************************************************************
 * 
 * AVIParserPBContextInit() - Initializes the AVI parser context for a
 *                             particular media file
 *
 * SYNOPSIS
 *       STATUS AVIParserPBContextInit (
 *          AVIPARSERPBCONTEXT      *pParserContext, 
 *          AVICONTENTHANDLE        hAVIFile, 
 *          FILEOPS                 *pFileOps
 *      )
 *   
 * INPUT
 *      pParserContext  - Pointer to AVI parser context structure to be
 *                        initialized
 *                         
 *      hAVIFile        - Handle to AVI file
 *        
 *      pFileOps        - Pointer to a structure containing file
 *                        operation callbacks 
 *       
 * DESCRIPTION
 *      The AVIParserPBConextInit() parses the header objects of AVI 
 *      file and initializes the AVIPARSERPBCONTEXTINTERNAL data 
 *      structures of the parser. The user shall allocate the memory to 
 *      the context structure and pass the address of the structure 
 *      through the AVIParserPBContextInit() call. The parser uses the 
 *      context structure pointer as the reference for the subsequent 
 *      operations on the AVI file. The AVIParserPBContextInit () shall 
 *      return appropriate error code while parsing the AVI file and
 *      initializing the internal AVI context structure.
 *       
 * RETURN
 *      AVI_SUCCESS         - Success. 
 *      AVI_E_INVALIDARG    - Parameter is not valid or is a NULL
 *                            pointer.
 *
 *      AVI_E_BADHEADER     - AVI header is improper.
 *      AVI_E_INVALIDSIZE   - Particular field size is invalid in the
 *                            AVI file.
 **********************************************************************/
  STATUS AVIParserContextInit (
    AVIPARSERPBCONTEXT      *pParserContext, 
    AVICONTENTHANDLE        hAVIFile, 
    FILEOPS                 *pFileOps
 );


 

/***********************************************************************
 * 
 * AVIGetNumStreams () - Retrieves the total no. of audio and video 
 *                          streams in an AVI file
 *                       
 * SYNOPSIS
 *       STATUS AVIGetNumStreams (
 *          AVIPARSERPBCONTEXTHANDLE        hAVIPbContext,
 *           BYTE                        *pNumAudStreams,
 *           BYTE                        *pNumVidStreams
 *      )
 * 
 * INPUT
 *      hAVIContext     - Handle to AVI parser context.
 *      pNumAudStreams   Pointer to hold the number of audio streams 
 *                          in the AVI content.
 *      pNumVidStreams   Pointer to hold the number of video streams 
 *                          in the AVI content. 
 *        
 * DESCRIPTION
 *      The AVIGetNumStreams () API retrieves the number of audio and 
 *      video streams in an AVI file.
 * 
 * RETURN
 *      AVI_SUCCESS                 - on success.
 *      AVI_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS AVIGetNumStreams (
    AVIPARSERPBCONTEXTHANDLE            hAVIPbContext,
     BYTE                            *pNumAudStreams,
     BYTE                            *pNumVidStreams
 );


/***********************************************************************
 * 
 * AVIGetStreamMinInfo() - Retrieves the minimum information about 
 *                      specific streams.
 *
 * SYNOPSIS
 *       STATUS AVIGetStreamMinInfo (
 *          AVIPARSERPBCONTEXTHANDLE    hAVIPbContext,
 *           INT32                    dwStreamType,
 *           BYTE                    cStreams,
 *          MINSTRMINFO                 *pMinStreamInfo[]
 *      )
 * 
 * INPUT
 *      hAVIContext      - Handle to AVI parser context.
 *      dwSteamType      - Type of the steam for which the minimum 
 *                          information to be retrieved.
 *      cStreams         - Number of streams for which the information 
 *                          be retrieved.
 *      pMinStreamInfo[]  Pointer to the array of MINSTRMINFO 
 *                          structure.
 * 
 * DESCRIPTION
 *      The AVIGetStreamMinInfo () API retrieves the minimum 
 *      information of audio or video streams which is useful for the 
 *      application for further operations like complete stream 
 *      information extraction and playback. It retrieves the minimum 
 *      information for the first cStreams in the AVI file.  
 *      The minimum information could be stream id, steam language, 
 *      bit rate and bit rate type (variable or constant). 
 *      Application shall pass the pointer to the array of 
 *      MINSTEARMINFO structure to hold the streams information.
 * 
 * RETURN
 *      AVI_SUCCESS                 - on success.
 *      AVI_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS AVIGetStreamMinInfo (
    AVIPARSERPBCONTEXTHANDLE      hAVIPbContext,
     INT32                      dwStreamType,
     BYTE                      cStreams,
    MINSTREAMINFO                 *pMinStreamInfo[]
 );

/***********************************************************************
 * 
 * AVIGetAudioStreamInfo() - Retrieves the information about specified 
 *                            audio stream specific streams.
 *
 * SYNOPSIS
 *       STATUS AVIGetAudioStreamInfo (
 *          AVIPARSERPBCONTEXTHANDLE                hAVIPbContext,
 *           BYTE                                bStreamId,
 *          AVIAUDIOSTREAMINFO                      *pAudStreamInfo
 *      )
 * 
 * INPUT
 *      hAVIContext    - Handle to AVI parser context.
 *      bSteamId       - Id of the audio stream for which the 
 *                          information to be retrieved.
 *      pAudStreamInfo  Pointer to the AVIAUDIOSTREAMINFO structure.
 * 
 * DESCRIPTION
 *      The AVIGetAudioStreamInfo () API retrieves the complete 
 *      information of audio which is useful for the application to 
 *      build the RCA header. Application can directly dump the whole 
 *      structure to the RCA header without any modification to the 
 *      contents of the structure. 
 * 
 * RETURN
 *      AVI_SUCCESS                 - on success.
 *      AVI_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS AVIGetAudioStreamInfo (
    AVIPARSERPBCONTEXTHANDLE                hAVIPbContext,
     BYTE                                bStreamId,
    AVIAUDIOSTREAMINFO                    *pAudStreamInfo
 );


/***********************************************************************
 * 
 * AFSGetVideoStreamInfo() - Retrieves the information about specified 
 *                              video stream.
 *
 * SYNOPSIS
 *       STATUS AVIGetVideoStreamInfo(
 *          AVIPARSERPBCONTEXTHANDLE        hAVIPbContext,
 *           BYTE                        bStreamId,
 *          AVIVIDEOSTREAMINFO              *pVidStreamInfo
 *      )
 * 
 * INPUT
 *      hAVIPbContext   - Handle to AVI parser context.
 *      bSteamId        - Id of the video stream for which the complete
 *                           information to be retrieved.
 *      pVidStreamInfo   Pointer to the AVIVIDEOSTREAMINFO structure.
 *
 * DESCRIPTION
 *      The AVIGetVideoStreamInfo() API retrieves the information about 
 *      the specified video stream which is useful for the application 
 *      to build the RCV header. Application shall pass the pointer to 
 *      the AVIVIDEOSTREAMINFO structure to hold the video stream 
 *      information.
 * RETURN
 *      AVI_SUCCESS                 - on success.
 *      AVI_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS AVIGetVideoStreamInfo (
    AVIPARSERPBCONTEXTHANDLE            hAVIPbContext,
     BYTE                            bStreamId,
    AVIVIDEOSTREAMINFO                  *pAudStreamInfo
 );


/***********************************************************************
 * 
 * AVISetActiveStream() - Notifies the AVI parser to parse the 
 *                          specified stream Id 
 *
 * SYNOPSIS
 *       STATUS AVISetActiveStream (
 *          AVIPARSERPBCONTEXTHANDLE    hAVIPbContext, 
 *          AVISTREAMTYPE               strmType,
 *           BYTE                    bActiveStreamId
 *      )
 *
 * INPUT
 *      hAVIPbContext       - Handle to AVI parser context. 
 *      strmType            - Type of the stream.
 *      bActiveStreamId     - Active audio or video stream Id.
 *
 * DESCRIPTION
 *      The AVISetActiveStream() API sets the Id audio or video stream 
 *      to parse (active stream). The AVIGetNextMediaObj () API will 
 *      retrieve the media object from the active stream in an AVI file.     
 * 
 * RETURN
 *      AVI_SUCCESS                 - on success.
 *      AVI_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS AVISetActiveStream (
    AVIPARSERPBCONTEXTHANDLE        hAVIPbContext, 
    AVISTREAMTYPE                   strmType,
     BYTE                        bActiveStreamId
 );



/***********************************************************************
 * 
 * AVIDecryptActiveStream() - Notifies the AVI parser to decrypt or not
 *                               the specified stream
 *
 * SYNOPSIS
 *       STATUS AVIDecryptActiveStream (
 *          AVIPARSERPBCONTEXTHANDLE    hAVIPbContext, 
 *          AVISTREAMTYPE               strmType,
 *           BOOL                     fDecrypt
 *      )
 * 
 * INPUT
 *      hAVIContext         - Handle to AVI parser context. 
 *      strmType            - Type of the stream.
 *      fDecrypt            - To be decrypted or not.
 * 
 * DESCRIPTION
 *      The AVIDecryptActiveStream() API specifies whether the active 
 *      stream to be decrypted or not. The AVIGetNextMediaObj() API 
 *      will retrieve the next media object from the active stream 
 *      after payload decryption of the media object.
 * 
 * RETURN
 *      AVI_SUCCESS                 - on success.
 *      AVI_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS AVIDecryptActiveStream (
    AVIPARSERPBCONTEXTHANDLE        hAVIPbContext, 
    AVISTREAMTYPE                   strmType,
     BOOL                         fDecrypt
 );



/***********************************************************************
 * 
 * AVIMetaGetAVITag() - Query the AVI header for various possible 
 *                          metadata.
 *
 * SYNOPSIS
 *       STATUS AVIMetaGetAVITag (
 *          AVIPARSERPBCONTEXTHANDLE        hAVIPbContext,
 *          METADATAOBJ                     *pAVITag[],
 *          UINT16                          cMetadata 
 *      )
 *
 * INPUT
 *      hAVIPbContext   - Handle to AVI parser context.
 *      pAVITag          pointer to an array of METADATAOBJ structures.
 *      cMetadata        number of elements in the METADATAOBJ 
 *                             structures array.
 *
 * DESCRIPTION
 *      The application shall invoke AVIGetMetaAVITag() to get the 
 *      metadata listed in Table 1. from an AVI content. Meta data is 
 *      passed in the metadata node structure. The AVIGetMetaAVITag() 
 *      API internally parses the AVI header objects to retrieve the 
 *      specified metadata request.
 *      The steps inside the function are as follows
 *      1.  From the pAVITag parameter decide which header objects to 
 *              parse in the AVI header.
 *      2.  Use the objects offset set in the AVIPARSERPBCONTEXT 
 *              structure in the AVIParserPBContextInit() API.
 *      3.  Parse the required header objects and extract the metadata 
 *              from the objects.
 * 
 * RETURN
 *      AVI_SUCCESS             - on success.
 *      AVI_E_INVALIDARG        - if a parameter is not valid or 
 *                                   is a NULL pointer.
 *      AVI_E_METAUNSUPPORTED   - if the Metadata is not supported.
 *      AVI_E_FIELDNOTFOUND     - if the particular field was not 
 *                                             found.
 *      AVI_E_INVALIDSIZE       - due to an invalid size being read.
 * 
 **********************************************************************/
  STATUS AVIMetaGetAVITag (
        AVIPARSERPBCONTEXTHANDLE             hAVIPbContext,
        METADATAOBJ                         *pAVITag,
         UINT16                           cMetadata 
 );


/***********************************************************************
 * 
 * AVIGetNextMediaObj() - Retrieve next media object from the given 
 *                          stream type.
 *
 * SYNOPSIS
 *       STATUS AVIGetNextMediaObj (
 *          AVIPARSERPBCONTEXTHANDLE        hAVIContext, 
 *           INT32                        strmType,
 *           BYTE                        *pbBuffer,
 *           BYTE                        *pcBuffer,
 *           UINT32                       dwStartOffset,
 *          MEDIAOBJMETAINFO                *pMediaObjMetaInfo
 *      )
 *
 * 
 * INPUT
 *      hAVIContext         - handle to AVI parser context. 
 *      strmType            - Type of the stream (audio or video).
 *      pbBuffer             pointer to the buffer to hold the block 
 *                                      data.
 *      pcBuffer             pointer to length of the pbBuffer.
 *      dwStartOffset       - Offset in the buffer at which the media 
 *                                  object to be copied.
 *      pMediaObjMetaInfo    Pointer to MEDIAOBJMETAINFO structure 
 *                                  containing information about 
 *                                  the media object.
 * 
 * 
 * DESCRIPTION
 *      The AVIGetNextMediaObj() API will retrieve a media object 
 *      from the active stream set by AVISetActiveStream() API. A media
 *      object might have split across multiple payloads in a single 
 *      data packet or in multiple data packets. The buffer to where 
 *      the data shall be copied should be allocated by the application
 *      before invoking the API. The length of the buffer should be at 
 *      least the size of a media object. If the length of the buffer 
 *      is less than the media object, the API shall return 
 *      AVI_E_BUFINSUFFICIENT error code. 
 *      The parser also returns the metadata of media object. 
 *      The metadata information will help the user build the RCV frame 
 *      header for video stream or he can copy the media object 
 *      information to a RCA file before dumping the actual media 
 *      object data. The payload metadata shall be returned in 
 *      MEDIAOBJMETAINFO structure allocated and managed by the user. 
 *      The payload data shall be decrypted if the file is DRM 
 *      protected and also if the fDecryptAudStream or 
 *      fDecryptVidStream members of the AVIPARSERPBCONTEXT is set.
 * 
 *      Steps inside the function     
 *      Let us consider an example of a media object being split across
 *      three payloads.
 *          1. Return AVI_E_BUFINSUFFICIENT error if the media object 
 *              size is more than the length of the buffer.
 *          2. Copy the payload1 to (pbBuffer + dwStartOffset) and the 
 *              decrypt the payload1.
 *          3. Copy the payload2 to (pbBuffer + dwStartOffset + 
 *              payloadSize1) and the decrypt the payload 2.
 *          4. Copy the payload3 to (pbBuffer + dwStartOffset + 
 *              payloadSize1 + payloadSize2) and the decrypt the 
 *              payload 3.
 *          5. Extract the payload header information from all the 
 *              three payloads and fill the MEDIAOBJMETAINOF structure.
 * 
 * RETURN
 *      AVI_SUCCESS            - on success.
 *      AVI_E_INVALIDARG       - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *      AVI_E_BADPACKETHEADER  - if it finds problems parsing an AVI 
 *                                      packet header.
 *      AVI_E_BADPAYLOADHEADER - if it finds problems parsing an AVI 
 *                                      payload header.
 *      AVI_E_BADDATAHEADER    - if it finds problems parsing an AVI 
 *                                       data object header.
 *      AVI_E_BUFINSUFFICIENT  - if the length of the buffer is less 
 *                                      than the media object.
 *      AVI_E_ENDOFFILE        - if it reaches the end of file.
 *
 **********************************************************************/
 STATUS AVIGetNextMediaObj (
        AVIPARSERPBCONTEXTHANDLE        hAVIContext, 
         INT32                        strmType,
         BYTE                        *pbBuffer,
         UINT32                        *pcBuffer,
         UINT32                       dwStartOffset,
        MEDIAOBJMETAINFO                *pMediaObjMetaInfo
 );
 /***********************************************************************
 * 
 * AVISeekToMS() - Seek to specified time position in the file.
 *
 * SYNOPSIS
 *       STATUS AVISeekToMS (
 *          AVIPARSERCONTEXTHANDLE          hAVIContext, 
 *           UINT32                       dwTimeOffsetInMS,
 *           INT32                        streamType
 *      ) 
 *
 * INPUT
 *      hAVIContext                 - Pointer to AVI Parser context 
 *                                       structure for a media file.
 *      dwTimeOffsetInMilliseconds  - Time to seek in milliseconds
 *
 * 
 * DESCRIPTION
 *      The AVISeekToMS() API will internally move both the audio and 
 *      video data parsing pointer to the specified file offset 
 *      calculated from seek value in milliseconds. The AVISeekToMS() 
 *      API will internally rounds down to start of the packet which 
 *      includes that time. It internally considers total playback 
 *      duration in milliseconds, per packet duration in milliseconds, 
 *      to calculate the data packet offset to jump to from the given 
 *      dwTimeOffsetInMS parameter. It internally compares the seek to 
 *      millisecond value with the presentation time of the media 
 *      object being present in the data packet at the calculated file 
 *      offset to jump to exact media object.
 *      If the given millisecond offset crosses the total playback time,
 *      the parser sets the currentFileOffset mark to last data packet. 
 *      The AVISeekToMS() API shall return error code if the user 
 *      tries backwards seeks.
 *
 * RETURN
 *      AVI_SUCCESS         - on success.
 *      AVI_E_INVALIDARG    - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *      AVI_E_FAIL          - if the time position is not able to set.
 *      AVI_E_ENDOFFILE     - if it reaches the end of file. 
 *
 **********************************************************************/
  STATUS AVISeekToMS (
        AVIPARSERPBCONTEXTHANDLE         hAVIContext, 
         UINT32                        dwTimeOffsetInMS,
         INT32                         streamType       
 );

#endif //_AVI_PARSER_H_
