/*
 * AVIParserInternal.h
 *
 * Header file for AVI Parser plugin - internal data structures
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */


#ifndef _AVI_PARSER_INTERNAL_H_
#define _AVI_PARSER_INTERNAL_H_

#include <dataTypes.h>
#include <AVIParserError.h>


// IFNDEF GET_BYTE defined in drmbytemanip.h
#ifndef AVIDRMDEFINE
#define GET_BYTE(pb,ib)             (pb)[(ib)]
#define PUT_BYTE(pb,ib,b)           (pb)[(ib)]=(b)
#endif // AVIDRMDEFINE 

#define LITTLE_ENDIAN_TARGET    1

#define AVI_BYT_CopyBytes(to,tooffset,from,fromoffset,count)    \
        _Memcpy(&((to)[(tooffset)]),&((from)[(fromoffset)]),(count))
            
#define AVI_BYT_MoveBytes(to,tooffset,from,fromoffset,count)    \
        _AVIMemmove(&((to)[(tooffset)]),&((from)[(fromoffset)]),(count))
            
#define AVI_BYT_SetBytes(pb,ib,cb,b)                            \
            _AVIMemset(&((pb)[(ib)]),b,cb)
            
#define AVI_BYT_CompareBytes(pbA,ibA,pbB,ibB,cb)                \
            _AVIMemcmp(&((pbA)[(ibA)]),&((pbB)[(ibB)]),(cb))
            

#define _AVIIsEqualGUID( rguid1, rguid2 )                       \
     ( ! AVI_BYT_CompareBytes( rguid1, 0,rguid2,0, sizeof(  GUID ) ) )

#if LITTLE_ENDIAN_TARGET == 1
#define BYTES_TO_DWORDOFFSET            BYTE2LITTLEENDIANOFFSET
#define BYTES_TO_WORDOFFSET             BYTE2LITTLEENDIANWORDOFFSET
#define DWORD_TO_BYTESOFFSET            DWORD2LITTLEENDIANOFFSET

#define BYTES_TO_QWORDOFFSET( qword, byte, i )          \
                    BYTES2QWORD_LEOFFSET( qword, byte, i )
                     
#define WORD_TO_BYTESOFFSET( byte, word, i )            \
                    WORD2BYTES_LEOFFSET( byte, word, i )
                    
#define QWORD_TO_BYTESOFFSET( byte, qword, i )          \
                    QWORD2BYTES_LEOFFSET( byte, qword, i )

#else /* for BIG_ENDIAN_TARGET */
#define BYTES_TO_DWORDOFFSET            BYTE2BIGENDIANOFFSET
#define BYTES_TO_WORDOFFSET             BYTE2BIGENDIANWORDOFFSET
#define DWORD_TO_BYTESOFFSET            DWORD2BIGENDIANOFFSET

#define BYTES_TO_QWORDOFFSET( qword, byte, i )          \
                    BYTES2QWORD_BEOFFSET( qword, byte, i )
                    
#define WORD_TO_BYTESOFFSET( byte, word, i )            \
                    WORD2BYTES_BEOFFSET( byte, word, i )
                    
#define QWORD_TO_BYTESOFFSET( byte, qword, i )          \
                    QWORD2BYTES_BEOFFSET( byte, qword, i )
#endif /* End TARGET_LITTLE_ENDIAN */


#define WORD2BYTES_BEOFFSET( byte, word, i )            \
            {FIX_ENDIAN_WORD((word));                   \
            AVI_BYT_CopyBytes(                          \
            (byte),(i),( BYTE *)&(word),0,sizeof( UINT16));      \
            FIX_ENDIAN_WORD((word));}
            
#define QWORD2BYTES_BEOFFSET( byte, qword, i )              \
            {FIX_ENDIAN_QWORD((qword));                     \
            AVI_BYT_CopyBytes(                              \
            (byte),(i),( BYTE *)&(qword), 0, sizeof( UINT64));    \
            FIX_ENDIAN_QWORD((qword));}
                  
#define BYTES2QWORD_BEOFFSET( qword, byte, i )                      \
            {AVI_BYT_CopyBytes(                                     \
            ( BYTE *)&(qword),0,(byte),(i),sizeof( UINT64));    \
            FIX_ENDIAN_QWORD((qword));}

#define WORD2BYTES_LEOFFSET( byte, word, i )        \
            AVI_BYT_CopyBytes(                      \
                (byte),(i),( BYTE *)&(word),0,sizeof( UINT16));
                
#define QWORD2BYTES_LEOFFSET( byte, qword, i )      \
            AVI_BYT_CopyBytes(                      \
                (byte),(i),( BYTE *)&(qword),0,sizeof( UINT64));
                
#define BYTES2QWORD_LEOFFSET( qword, byte, i)       \
            AVI_BYT_CopyBytes(                      \
                ( BYTE *)&(qword),0,(byte),(i),sizeof( UINT64));


#define BYTE2LITTLEENDIANOFFSET(dword, byte,i)      \
            AVI_BYT_CopyBytes(                      \
                ( BYTE *)&(dword),0,(byte),(i),sizeof( UINT32));
                
#define BYTE2LITTLEENDIANWORDOFFSET(word, byte,i)   \
            AVI_BYT_CopyBytes(                      \
                ( BYTE *)&(word),0,(byte),(i),sizeof( UINT16));
                
#define DWORD2LITTLEENDIANOFFSET( byte, dword,i )   \
            AVI_BYT_CopyBytes(                      \
                (byte),(i),( BYTE *)&(dword),0,sizeof( UINT32));
                
#define BYTE2BIGENDIANWORDOFFSET(word, byte,i)      \
            {AVI_BYT_CopyBytes(                     \
                ( BYTE *)&(word),0,(byte),i,sizeof( UINT16));    \
                FIX_ENDIAN_WORD((word));}
                
#define BYTE2BIGENDIANOFFSET(dword, byte,i)         \
            {AVI_BYT_CopyBytes(                     \
                ( BYTE *)&(dword),0,(byte),i,sizeof( UINT32));  \
                FIX_ENDIAN_DWORD((dword));}
                
#define DWORD2BIGENDIANOFFSET(byte, dword,i)        \
            {FIX_ENDIAN_DWORD((dword));             \
             AVI_BYT_CopyBytes(                     \
                (byte),(i),( BYTE *)&(dword),0,sizeof( UINT32)); \
                FIX_ENDIAN_DWORD((dword)); }



#define _loadBYTEOffset( b, p, i )       b = GET_BYTE(p, i);    \
                                            i += sizeof ( BYTE);
                                        
#define _saveBYTEOffset( p, b, i )       PUT_BYTE(p, i, b);     \
                                            i += sizeof ( BYTE);
                                            
#define _loadWORDOffset( w, p, i )       BYTES_TO_WORDOFFSET(   \
                                            (w),(p), (i) );     \
                                            (i) += sizeof(  UINT16 )
                                        
#define _saveWORDOffset( p, w, i )       WORD_TO_BYTESOFFSET(   \
                                            (p), (w), (i) );    \
                                            (i) += sizeof(  UINT16 )
                                        
#define _saveWORDOffset( p, w, i )       WORD_TO_BYTESOFFSET(   \
                                            (p), (w), (i) );    \
                                            (i) += sizeof(  UINT16 )
                                        
#define _loadDWORDOffset( dw, p, i )     BYTES_TO_DWORDOFFSET(  \
                                            (dw), (p), (i) );   \
                                            (i) += sizeof(  UINT32 )
                                        
#define _saveDWORDOffset( p, dw, i )     DWORD_TO_BYTESOFFSET(  \
                                            (p), (dw), (i) );   \
                                            (i) += sizeof(  UINT32 )
                                            
#define _loadQWORDOffset( qw, p, i )     BYTES_TO_QWORDOFFSET(  \
                                            (qw), (p), (i) );   \
                                            (i) += sizeof(  UINT64 )
                                        
#define _saveQWORDOffset( p, qw, i )     QWORD_TO_BYTESOFFSET(  \
                                            (p), (qw),(i) );    \
                                            (i) += sizeof(  UINT64 )
                                         

#define _loadAVIGUID( g, p, i )                             \
        {                                                   \
            _loadDWORDOffset( (g).Data1, p, i );                  \
            _loadWORDOffset( (g).Data2, p, i );                   \
            _loadWORDOffset( (g).Data3, p, i );             \
            PUT_BYTE((g).Data4, 0, GET_BYTE(p, i));         \
            PUT_BYTE((g).Data4, 1, GET_BYTE(p, (i + 1)));   \
            PUT_BYTE((g).Data4, 2, GET_BYTE(p, (i + 2)));   \
            PUT_BYTE((g).Data4, 3, GET_BYTE(p, (i + 3)));   \
            PUT_BYTE((g).Data4, 4, GET_BYTE(p, (i + 4)));   \
            PUT_BYTE((g).Data4, 5, GET_BYTE(p, (i + 5)));   \
            PUT_BYTE((g).Data4, 6, GET_BYTE(p, (i + 6)));   \
            PUT_BYTE((g).Data4, 7, GET_BYTE(p, (i + 7)));   \
            i += 8;                                         \
        }



#define AVI_INT64Add(a, b)      ( (a) + (b) )
#define AVI_INT64Sub(a, b)      ( (a) - (b) )
#define AVI_INT64Mul(a, b)      ( (a) * (b) )
#define AVI_INT64Div(a, b)      ( (a) / (b) )
#define AVI_INT64Mod(a, b)      ( (a) % (b) )
#define AVI_INT64And(a, b)      ( (a) & (b) )
#define AVI_INT64ShR(a, b)      ( (a) >> (b) )
#define AVI_INT64ShL(a, b)      ( (a) << (b) )
#define AVI_INT64Eql(a, b)      ( (a) == (b) )
#define AVI_INT64Les(a, b)      ( (a) < (b) )
#define AVI_INT64(b)            ( ( INT64) (b) )
#define AVI_INT64Asgn(a, b)     ((( INT64)(a)<<32) & (b))
#define AVI_UINT32INT64(b)      (( INT64)(b))

#define AVI_INT64ToUINT32(b)    (( UINT32)(b))


#define AVI_UINT64Add(a, b)     ( (a) + (b) )
#define AVI_UINT64Sub(a, b)     ( (a) - (b) )
#define AVI_UINT64Mul(a, b)     ( (a) * (b) )
#define AVI_UINT64Div(a, b)     ( (a) / (b) )
#define AVI_UINT64Mod(a, b)     ( (a) % (b) )
#define AVI_UINT64And(a, b)     ( (a) & (b) )
#define AVI_UINT64ShR(a, b)     ( (a) >> (b) )
#define AVI_UINT64ShL(a, b)     ( (a) << (b) )
#define AVI_UINT64Eql(a, b)     ( (a) == (b) )
#define AVI_UINT64Les(a, b)     ( (a) < (b) )
#define AVI_UINT64(b)           ( ( UINT64) (b) )


#define SIZEOF_U8                                       1

#define AVI_MIN_OBJ_SIZE                                24
#define AVI_DATA_OBJECT_SIZE                            50

#define AVI_RIGHT_PLAYBACK_MASK                         0xFFFE
#define AVI_RIGHT_COLLABORATIVE_PLAY_MASK               0xFFFD
#define AVI_RIGHT_COPY_CD_MASK                          0xFFFB
#define AVI_RIGHT_COPY_MASK                             0xFFF7
#define AVI_RIGHT_COPY_TO_SDMI_DEVICE_MASK              0xFFF3
#define AVI_RIGHT_COPY_TO_NON_SDMI_DEVICE_MASK          0xFFEF
#define AVI_RIGHT_BACKUP_MASK                           0xFFDF
#define AVI_RIGHT_PLAYLISTBURN_MASK                     0xFFBF

typedef  VOID*                            AVICONTENTHANDLE;

/*
 * Stream parsing state 
 */
typedef enum 
{
    P_NOT_VALID = 0,
    P_NEW_PACKET,
    P_MIDDLE_PACKET
} AVIPARSESTATE;


/*
 * Structure contaning the AVI Data packet header information
 */
typedef struct 
{
     BOOL         fParityPacket;
     BOOL         fEccPresent;
     BOOL         fMultiPayloads;
     UINT32       dwParseOffset;
     BYTE        bECLen;
     BYTE        bPacketLenType;
     BYTE        bPadLenType;
     BYTE        bSequenceLenType;
     UINT32       dwPacketLenTypeOffset;
     BYTE        bOffsetBytes;
     BYTE        bOffsetLenType;
     BYTE        bPayLenType;
     BYTE        bPayBytes;
     UINT32       dwPacketLenOffset;
     UINT32       dwExplicitPacketLength; 
     UINT32       dwSequenceOffset;
     UINT32       dwSequenceNum; 
     UINT32       dwPadLenOffset;
     UINT32       dwPadding;
     UINT32       dwSCR;
     UINT32       dwPayLenTypeOffset;
     UINT32       cPayloads;
     UINT32       cPayloadsLeft;
     UINT16       wDuration;
} AVIPACKETPARSEINFO;

/*
 * Structure contaning the AVI payload header information
 */
typedef struct 
{
     UINT16       wPacketOffset;
     UINT16       wTotalSize;

     BYTE        bStreamId;
     BYTE        bObjectId;
     BYTE        bRepData;
     BYTE        bJunk;

     UINT32       dwObjectOffset;
     UINT32       dwObjectSize;
     UINT32       dwObjectPres;

     BOOL         fIsKeyFrame;
     BOOL         fIsCompressedPayload;
    /* 
     * Offset of this payload from the start of the data packet 
     */
     UINT32       dwPayloadOffset;
     UINT32       dwPayloadSize;
     UINT16       wTotalDataBytes;
     UINT32       dwDeltaPresTime;

     UINT16       wBytesRead;
     BYTE        bSubPayloadState;
     BYTE        bNextSubPayloadSize;
     UINT16       wSubpayloadLeft;
     UINT16       wSubCount;
} AVIPAYLOADPARSEINFO;

/*
 * Structure containing parsing information about active audio stream
 */
typedef struct 
{
    /*
     * Number of audio or video streams in the AVI file 
     */
     UINT32               cStreams;
    
    /*
     * Active  stream number 
     */
     BYTE					bActiveStreamId;
    
    /*
     * Decrypt the stream or not 
     */
     BOOL                 fDecryptStream;

    /*
     * DRM protected content
     */
     BOOL                 fProtectedContent;
    
    /*
     * Current data packet offset
     */
     UINT32               dwCurrentPacketOffset;
    
    /*
     * Stream Next packet offset
     */
     UINT32               dwNextPacketOffset;
    

	// Stream Index offset
	 INT32				dwCurrentIndexOff;


    /*
     * In the middle of a data packet
     */
    AVIPARSESTATE           ParseState;
    
    /*
     * Whether the seek point is set
     */
     BOOL                 fSeekPtSet;
    
    /*
     * Packet header information
     */
    //AVIPACKETPARSEINFO      AVIPacketInfo;
    
    /*
     * payload header information
     */ 
    //AVIPAYLOADPARSEINFO     AVIPayloadInfo; 
    
    /*
     * Payload header has been parsed
     */   
    // BOOL                 fPayloadParsed; 
} STREAMPARSEINFO;


/* Prototype of the read callback function */
typedef  STATUS (*readCb)(AVICONTENTHANDLE  AVIHandle, 
                         BYTE **pbBuffer,  UINT32 fileOffset, 
                         UINT32 cBytes);
/* Prototype of the write callback function */                        
typedef  STATUS (*writeCb)(AVICONTENTHANDLE AVIHandle, 
                          BYTE *pbBuffer,  UINT32 fileOffset, 
                          UINT32 cBytes);



/* 
 * State of the media stream
 */
typedef struct
{
   
    BYTE     bStreamId;
   
    BOOL      fEncrypted; 
} STREAMSTATE;

#ifdef AVIDRMDEFINE
#include <drmcommon.h>
#include <drmutilities.h>
#include <drmcontextsizes.h>
#include <drmmanager.h>
#endif // AVIDRMDEFINE

typedef struct _avimainheader {
	 UINT32  fcc;
	 UINT32  cb;
	 UINT32  dwMicroSecPerFrame;
	 UINT32  dwMaxBytesPerSec;
	 UINT32  dwPaddingGranularity;
	 UINT32  dwFlags;
	 UINT32  dwTotalFrames;
	 UINT32  dwInitialFrames;
	 UINT32  dwStreams;
	 UINT32  dwSuggestedBufferSize;
	 UINT32  dwWidth;
	 UINT32  dwHeight;
	 UINT32  dwReserved[4];
} AVIMAINHEADER,*PAVIMAINHEADER;

typedef struct 
{
    /*
     * Pointer I/O read function
     */
    readCb                              read;
     
    /*
     * Pointer I/O write function
     */
    writeCb                             write;    
    
    /*
     * Opaque handle to an AVI file 
     */
    AVICONTENTHANDLE                    hAVIContent;    
    
    /*
     * Container Header size
     */
     UINT32                           dwHeaderSize;

    
	// Container Header offset
     INT32							dwHeaderOffset;
	    
    /*
     * Container Media Data Size
     */
     UINT32                           dwDataSize;


    // Container Data START offset
     INT32							dwDataStartOffset;

    /*
     * Container Index size
     */
     UINT32                           dwIndexSize;
		

	// Container Index offset
	 UINT32							dwIndexStartOffset;

	// Container Index offset
	 UINT32							dwIndexAddendum;

    /*
     * Structure containing the audio stream parsing information
     */
    STREAMPARSEINFO                     audioParseInfoObj;
    
    /*
     * Structure containing the video stream parsing information
     */
    STREAMPARSEINFO                     videoParseInfoObj;



    
	
	
	/*
     * Byte offset of the first packet
     */
//     UINT32                           dwFirstPacketOffset;

    /*
     * Last packet offset in the file
     */
//     UINT32                           dwLastPacketOffset;

	
	
	
	
	
	/*
     * AVI File Size
     */
    // UINT32                           dwFileSize;

    /*
     * Total number of data packets in the AVI file 
     */
    // UINT64                           cDataPackets;
    
    /*
     * Size of the Data packet
     */
    // UINT32                           dwPacketLength;
    
    /*
     * Size of the AVI file
     */
    // UINT64                           qwFileSize;
         
    /*
     * Total Play time in milliseconds 
     */
     UINT64							qwDuration;
    
    /*
     * Specifies amount of time to buffer the data before starting to
     * play
     */
     UINT64                           qwPreroll;
    

    
    /*
     * Current offset in the file 
     */
    // INT32                            lCurrentFileOffset;
    
    
    /*
     * Offset of the next packet in the file to parse 
     */
    // UINT32                           dwNextPacketOffset;
    
    /*
     * DRM protected content
     */
    // BOOL                             fProtectedContent;
    
    
    /*
     * Structure containing the offsets of the AVI objects 
     */
    //AVIOBJSOFFSET                       AVIObjsOffset;

	//AVIMAINHEADER						aviMainHeaderInfo;

#ifdef AVIDRMDEFINE

    /*
     * Decrypt context for a AVI content 
     */
    DRM_MANAGER_DECRYPT_CONTEXT        *pDecryptContext; 
    
    /*
     * DRM session context for an AVI content 
     */
    DRM_MANAGER_CONTEXT                *pManagerContext; 
#endif // AVIDRMDEFINE

} AVIPARSERPBCONTEXTINTERNAL;

#endif //_AVI_PARSER_INTERNAL_H_
