export LD_LIBRARY_PATH=.:/opt/system_files_gstreamer:/opt/gstreamer/lib
export GST_PLUGIN_PATH=/opt/gstreamer/lib/gstreamer-0.10/
export PATH=$PATH:/opt/gstreamer/bin

gst-launch-0.10 filesrc location=$1 ! tidemux_asf ! adecoder Codec=2 ! osssink
