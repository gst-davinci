#
#  ======== xdccfg.mak ========
#  Variables and make rules that let a Linux build application incorporate
#  the XDC configuration step for its .cfg file

all:

# for config file <dir>/<name>.cfg, create temporary package in <dir>/<name>_package/
XDC_CFGPKGDIR = $(dir $(XDC_CFGFILE))$(basename $(notdir $(XDC_CFGFILE)))_package

# XDC executable
XDC_EXE = $(XDC_INSTALL_DIR)/xdc

# define compile flags the user must add to his CPPFLAGS
XDC_FLAGS=-Dxdc_target__='<gnu/targets/std.h>' \
	-I$(subst ;, -I,$(XDC_PATH)) -I$(XDC_CFGPKGDIR) -I$(XDC_CFGPKGDIR)/.. \
	-I$(XDC_INSTALL_DIR)/packages -I$(XDC_INSTALL_DIR)/packages/xdc
# define the generated C file, resulting Object file, and generated link file
XDC_CFILE=package/cfg/pkg_x470MV.c
XDC_OFILE=$(XDC_CFGPKGDIR)/$(patsubst %.c,%.o,$(XDC_CFILE))
XDC_LFILE=$(XDC_CFGPKGDIR)/$(patsubst %.c,%.xdl,$(XDC_CFILE))

# define the "debug" variant of the same
XDC_CFILED=package/cfg/pkgd_x470MV.c
XDC_OFILED=$(XDC_CFGPKGDIR)/$(patsubst %.c,%.o,$(XDC_CFILED))
XDC_LFILED=$(XDC_CFGPKGDIR)/$(patsubst %.c,%.xdl,$(XDC_CFILED))

# this rule makes XDC generate the necessary C file for "release" mode
$(XDC_CFGPKGDIR)/$(XDC_CFILE): $(XDC_CFGFILE) $(XDC_CFGPKGDIR)/package.xdc
	cd $(XDC_CFGPKGDIR); $(XDC_EXE) XDCPATH="$(XDC_PATH)" $(XDC_CFILE)

# this rule makes XDC generate the necessary C file for "debug" mode
$(XDC_CFGPKGDIR)/$(XDC_CFILED): $(XDC_CFGFILE) $(XDC_CFGPKGDIR)/package.xdc
	cd $(XDC_CFGPKGDIR); $(XDC_EXE) XDCPATH="$(XDC_PATH)" $(XDC_CFILED)

# create a dummy package for the program for purpose of running configuration
CONFIGBLDTEXT =                                         \
    var MVArm9 = xdc.useModule("gnu.targets.MVArm9");   \
    MVArm9.rootDir = "/dummy";                          \
    MVArm9.GCCVERS = "3.4.3";                           \
    MVArm9.GCCTARG = "armv5tl-montavista-linuxeabi";    \
    Build.targets = [ MVArm9 ];
PACKAGEBLDTEXT = \
    var targ = xdc.om[ "gnu.targets.MVArm9" ];          \
    Pkg.addExecutable( "pkg", targ, "ti.platforms.evmDM6446", { profile: "release" } );   \
    Pkg.addExecutable( "pkgd", targ, "ti.platforms.evmDM6446", { cfgScript: "pkg.cfg", profile: "debug" } );
PKGCFGTEXT = utils.importFile( "../$(notdir $(XDC_CFGFILE))" );
PACKAGEXDCTEXT =                          \
    requires ti.sdo.linuxutils.cmem;      \
    requires ti.sdo.ce.trace;             \
    requires dsplink.gpp;
$(XDC_CFGPKGDIR)/package.xdc:
	@rm -rf $(XDC_CFGPKGDIR)
	@mkdir $(XDC_CFGPKGDIR)
	@echo '$(CONFIGBLDTEXT)'  > $(XDC_CFGPKGDIR)/config.bld
	@echo '$(PACKAGEBLDTEXT)' > $(XDC_CFGPKGDIR)/package.bld
	@echo '$(PKGCFGTEXT)'     > $(XDC_CFGPKGDIR)/pkg.cfg
	@echo '$(PACKAGEXDCTEXT)' > $(XDC_CFGPKGDIR)/package.xdc
	@echo 'package {}'       >> $(XDC_CFGPKGDIR)/package.xdc

clean::
	# [CE] clean up XDC config stuff
	rm -rf $(XDC_CFGPKGDIR)
