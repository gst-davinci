/*
 *  ======== ceapp.c ========
 *  The CodecEngine-using part of the Linux application, separated from the
 *  rest of the Linux app to simplify the interface towards the rest of
 *  the main app. CodecEngine API and especially APIs for individual codecs
 *  are much more capable than what we need for simple (dummy) video
 *  encode/decode.
 */

/* Standard Linux headers */
#include <stdio.h>   // always include stdio.h
#include <stdlib.h>  // always include stdlib.h
#include <string.h>  // memset, memcpy, strcmp, strcpy

/* include various CodecEngine header files */
#include <xdc/std.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/video/viddec.h>
#include <ti/sdo/ce/audio/auddec.h>
#include <ti/sdo/ce/osal/Memory.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/utils/trace/TraceUtil.h>


/*  define handles to the CodecEngine and codecs to be used across ceapp_*
 *  function calls; we are not reentrant.
 */
static String engineName = "MPEG4Engine";

/*
 *  ======== ceapp_init ========
 */
void __libce_dummy(void)
{
    char *buf;
    int bufSize;

  Engine_Handle engineHandle  = NULL;
  VIDDEC_Handle viddecHandle = NULL;
  AUDDEC_Handle auddecHandle = NULL;

  VIDDEC_InArgs           vid_inArgs;	
  VIDDEC_OutArgs          vid_outArgs;
  XDM_BufDesc             inBufDesc;
  XDM_BufDesc             outBufDesc;	
  VIDDEC_Status vid_status;
  VIDDEC_DynamicParams vid_dynParams;


    AUDDEC_InArgs          aud_inArgs;	// input args for AUDDEC_process
    AUDDEC_OutArgs          aud_outArgs;	// output (return) from AUDDEC_process
    AUDDEC_Status aud_status;
    AUDDEC_DynamicParams aud_dynParams;


    /* initialize Codec Engine runtime first */
    CERuntime_init();

    engineHandle = Engine_open(engineName,NULL,NULL);

    /* activate DSP trace collection thread */
    TraceUtil_start(engineName);

    auddecHandle = AUDDEC_create(engineHandle, "invalid", NULL);
    AUDDEC_process(auddecHandle, &inBufDesc, &outBufDesc, &aud_inArgs, &aud_outArgs);
    AUDDEC_control(auddecHandle, XDM_GETSTATUS, &aud_dynParams, &aud_status);
    AUDDEC_delete(auddecHandle);



    viddecHandle = VIDDEC_create(engineHandle, "invalid", NULL);
    VIDDEC_process(viddecHandle, &inBufDesc, &outBufDesc, &vid_inArgs, &vid_outArgs);
    VIDDEC_control(viddecHandle, XDM_GETSTATUS, &vid_dynParams, &vid_status);
    VIDDEC_delete(viddecHandle);

    TraceUtil_stop();  /* close tracing thread */


    Engine_close(engineHandle);

    buf = (char *)Memory_contigAlloc(bufSize, Memory_DEFAULTALIGNMENT);

    Memory_contigFree(buf, bufSize);

}


