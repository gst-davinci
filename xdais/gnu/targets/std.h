/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== gnu/targets/std.h ========
 *
 */

#ifndef gnu_targets_STD_
#define gnu_targets_STD_

/*
 * xdc__LONGLONG__ indicates if compiler supports 'long long' type
 * xdc__BITS<n> __ indicates if compiler supports 'uint<n>_t' type
 */
#define xdc__LONGLONG__
#define xdc__BITS8__
#define xdc__BITS16__
#define xdc__BITS32__

/*
 *  ======== [U]Int<n> ========
 */
typedef signed char     xdc_Int8;
typedef unsigned char   xdc_UInt8;
typedef short           xdc_Int16;
typedef unsigned short  xdc_UInt16;
typedef long            xdc_Int32;
typedef unsigned long   xdc_UInt32;

/*
 *  ======== Bits<n> ========
 */
typedef unsigned char   xdc_Bits8;
typedef unsigned short  xdc_Bits16;
typedef unsigned char   xdc_Bits32;

/*
 *  ======== Arg ========
 */
typedef long            xdc_Arg;

#endif /* gnu_targets_STD_ */

/*
 *  @(#) gnu.targets 1, 0, 0, 0,134 1-2-2006 xdc-m52
*/

