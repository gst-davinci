/* 
 *  Copyright 2006
 *  Texas Instruments Incorporated
 *
 *  All rights reserved.  Property of Texas Instruments Incorporated
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 * 
 */

/**
 *  @file       iaudio.h
 *
 *  @brief      This header defines all types, constants, enums, and functions
 *              that are common across the various audio codecs.
 *
 *  @version    0.5
 */
/**
 *  @defgroup   IAUDIO    xDM Audio Interface
 *
 *  This is the xDM audio interface shared between the various codecs.
 */

#ifndef IAUDIO_
#define IAUDIO_

#ifdef __cplusplus
extern "C" {
#endif

/**
 *  @brief      xDM supported audio channel configurations.
 */
typedef enum {
    IAUDIO_MONO = 0,    /**< Single channel */
    IAUDIO_STEREO,      /**< Two channel */
    IAUDIO_THREE_ZERO,  /**< Three channel */
    IAUDIO_FIVE_ZERO,   /**< Five channel */
    IAUDIO_FIVE_ONE,    /**< 5.1 channel */
    IAUDIO_SEVEN_ONE    /**< 7.1 channel */
} IAUDIO_ChannelId;

/**
 *  @brief      xDM supported audio PCM formats
 */
typedef enum {
    /**
     *  Left channel data followed by right channel data.
     *  Note, for single channel (mono), right channel data will be the same
     *  as the left channel data.
     */
    IAUDIO_BLOCK = 0,

    /**
     *  Left and right channel data interleaved.
     *  Note, for single channel (mono), right channel data will be the same
     *  as the left channel data.
     */
    IAUDIO_INTERLEAVED
} IAUDIO_PcmFormat;



#ifdef __cplusplus
}
#endif

/*@}*/  /* ingroup */

#endif  /* IAUDIO_ */
/*
 *  @(#) ti.xdais.dm 1, 0, 0, 0,20 4-20-2006 dais-f06
*/

