/*
//============================================================================

//

//    FILE NAME : IVC1DEC.h

//

//    ALGORITHM : VC1DEC

//

//    VENDOR    : TI

//

//    TARGET DSP: C64x

//

//    PURPOSE   : IVC1DEC Interface Header

//

//============================================================================
*/
/* ------------------------------------------------------------------------ */
/* This software is protected by certain intellectual property rights of    */
/* Microsoft and cannot be used or further distributed without a license    */
/* from Microsoft                                                           */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2005 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


#ifndef IVC1DEC_
#define IVC1DEC_


#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/dm/ividdec.h>
//ATEME patch #include <idma3.h>


/*
// ===========================================================================
// IVC1DEC_Handle
//
// This handle is used to reference all VC1DEC instance objects
*/
typedef struct IVC1DEC_Obj *IVC1DEC_Handle;

/*
// ===========================================================================
// IVC1DEC_Obj
/
// This structure must be the first field of all VC1DEC instance objects
*/
typedef struct IVC1DEC_Obj {
    struct IVC1DEC_Fxns *fxns;
} IVC1DEC_Obj;

/*
// ===========================================================================
// IVC1DEC_Cmd
//
// The Cmd enumeration defines the control commands for the VC1DEC
// control method.
*/

typedef IVIDDEC_Cmd IVC1DEC_Cmd;

/*
// ===========================================================================
// control method commands
*/
#define IVC1DEC_GETSTATUS      XDM_GETSTATUS
#define IVC1DEC_SETPARAMS      XDM_SETPARAMS
#define IVC1DEC_RESET          XDM_RESET
#define IVC1DEC_SETDEFAULT     XDM_SETDEFAULT
#define IVC1DEC_FLUSH          XDM_FLUSH
#define IVC1DEC_GETBUFINFO     XDM_GETBUFINFO

/*
// ===========================================================================
// IVC1DEC_Params
//
// This structure defines the creation parameters for all VC1DEC objects
*/

typedef struct IVC1DEC_Params {
    IVIDDEC_Params   viddecParams;  //Should be the first argument

} IVC1DEC_Params;

/*
// ===========================================================================
// IVC1DEC_Status
//
// Status structure defines the parameters that can be changed or read
// during real-time operation of the alogrithm.
*/

typedef struct IVC1DEC_Status {
  IVIDDEC_Status viddecStatus;   //Should be the first argument

} IVC1DEC_Status;

/*
// ===========================================================================
// IVC1DEC_PARAMS
//
// Default parameter values for VC1DEC instance objects
*/
extern IVC1DEC_Params IVC1DEC_PARAMS;

/*
// ===========================================================================
// IVC1DEC_DynamicParams
//
// This structure defines the run time parameters for all VC1DEC objects
*/
typedef struct IVC1DEC_DynamicParams {
  	IVIDDEC_DynamicParams viddecDynamicParams; //Should be the first argument
    XDAS_UInt8      bIsElementaryStream;        //Determines VC1/RCV

} IVC1DEC_DynamicParams;

/*
// ===========================================================================
// IVC1DEC_InArgs
//
// This structure provides the Input parameters for VC1 Decode call
*/
typedef struct IVC1DEC_InArgs {
    IVIDDEC_InArgs viddecInArgs;          //Should be the first argument

} IVC1DEC_InArgs;

/*
// ===========================================================================
// IVC1DEC_OutArgs
//
// This structure provides the Output parameters for VC1 Decode call
*/
typedef struct IVC1DEC_OutArgs {
   	IVIDDEC_OutArgs viddecOutArgs;      //Should be the first argument

} IVC1DEC_OutArgs;

/*
 *  ======== IVC1DEC_Fxns ========
 *  This structure defines all of the operations on VC1DEC objects
 */

typedef struct IVC1DEC_Fxns {

	IVIDDEC_Fxns ividdec;

} IVC1DEC_Fxns;


#endif	/* IVC1DEC_ */

/* This software is protected by certain intellectual property rights of Microsoft and
                              cannot be used or further distributed without a license from Microsoft */



