/*
===============================================================================
* Copyright (c) Texas Instruments Inc 2003, 2004
*
* Use of this software is controlled by the terms and conditions found
in the license agreement under which this software has been supplied
===============================================================================
*/

/*c
===============================================================================
	date           : 23Oct2003
	file_name      : iwma.h
	purpose        : To define all parameters and functions supported.
	author         : SBP - Sreenu Babu P (sreenu@ti.com)
===============================================================================
c*/

/*c
===============================================================================
* Revision History
-------------------------------------------------------------------------------
Notes:
Ver    - 1.0

* Author-Date		Ver		Changes
-------------------------------------------------------------------------------
* SBP-23Oct2003	1.0		Initial version
===============================================================================
c*/

/*=============================================================================
defines and macros
-----------------------------------------------------------------------------*/
#ifndef IWMA_
#define IWMA_

/*=============================================================================
includes
-----------------------------------------------------------------------------*/
#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/xdas.h>
#include <ti/xdais/dm/iauddec.h>


/*=============================================================================
 Data type declaration
-----------------------------------------------------------------------------*/
/* Commented for clean-up

#if defined _WIN32
#include <windows.h>
#define _BOOL_DEFINED
#endif
*/

#define WMA_NONSDMI_LIC 0x02
#define WMA_SDMI_LIC    0x10
#define WMA_BURNCD_LIC  0x08


#define PACKED
#define WMA_MAX_DATA_REQUESTED  128
//***********************************************************************
// Exercising Rapier Special Modes, not everything available all the time
//***********************************************************************
#define DECOPT_CHANNEL_DOWNMIXING      0x00000001
#define DECOPT_DRC                     0x00000002
#define DECOPT_INTERPOLATED_DOWNSAMPLE 0x00000004
#define DECOPT_HALF_TRANSFORM          0x00000008
#define DECOPT_HALF_UP_TRANSFORM       0x00000010
#define DECOPT_2X_TRANSFORM            0x00000020
#define DECOPT_REQUANTTO16             0x00000040
#define DECOPT_DOWNSAMPLETO44OR48      0x00000080
#define DECOPT_LTRTDOWNMIX             0x00000100

/* Coded specific Extended Errors*/
#define XDMS_ISFAILED(x)         (((x)) & 0x1)
#define XDMS_ISNOMOREFRAMES(x)         (((x)>>1) & 0x1)

/* 8-bit unsigned type  */
#ifndef _WMAU8_DEFINED
#define _WMAU8_DEFINED
typedef unsigned char tWMA_U8;
#endif /* _WMAU8_DEFINED */

/* 8-bit signed type  */ //PSB
#ifndef _WMAI8_DEFINED
#define _WMAI8_DEFINED
typedef char tWMA_I8;
#endif /* _WMAU8_DEFINED */ //PSB

/* 16-bit signed type  */
#ifndef _WMAI16_DEFINED
#define _WMAI16_DEFINED
typedef short tWMA_I16;
#endif /* _WMAI16_DEFINED */

/* 16-bit unsigned type  */
#ifndef _WMAU16_DEFINED
#define _WMAU16_DEFINED
typedef unsigned short tWMA_U16;
#endif /* _WMAU16_DEFINED */

/* 32-bit unsigned type  */
#ifndef _WMAU32_DEFINED
#define _WMAU32_DEFINED
typedef unsigned int tWMA_U32;
#endif /* _WMAU32_DEFINED */

/* 32-bit type  */
#ifndef _WMAI32_DEFINED
#define _WMAI32_DEFINED
typedef int tWMA_I32;
#endif /* _WMAI32_DEFINED */

struct I64Struct_iwma
{
	tWMA_I32 High;
	tWMA_U32 Low;
};
struct U64Struct_iwma
{
	tWMA_U32 High;
	tWMA_U32 Low;
};
typedef struct U64Struct_iwma U64_iwma;
typedef struct I64Struct_iwma I64_iwma;

/* Bool */
#ifndef _WMABool_DEFINED
#define _WMABool_DEFINED
typedef int tWMA_Bool;
#endif /* _WMABool_DEFINED */

/* 64-bit unsigned type  */
#ifndef _WMAU64_DEFINED
#define _WMAU64_DEFINED
#if defined(macintosh) || defined(_Embedded_x86)
typedef unsigned long long  tWMA_U64;
#else
typedef U64_iwma    tWMA_U64;
#endif
#endif /* _WMAU64_DEFINED */

/* 64-bit signed type  */
#ifndef _WMAI64_DEFINED
#define _WMAI64_DEFINED
#if defined(macintosh) || defined(_Embedded_x86)
typedef long long  tWMA_I64;
#else
typedef I64_iwma    tWMA_I64;
#endif
#endif /* _WMAI64_DEFINED */

#ifndef _PCMFORMAT_DEFINED
#define _PCMFORMAT_DEFINED
typedef struct _PCMFormat PCMFormat;
#endif //_PCMFORMAT_DEFINED


#ifndef _QWORD_DEFINED
#define _QWORD_DEFINED
typedef PACKED struct tQWORD
{
    tWMA_U32   dwLo;
    tWMA_U32   dwHi;

}   QWORD;
#endif /* _QWORD_DEFINED */
#ifndef GUID_DEFINED
#define GUID_DEFINED

typedef struct {          // size is 16
    tWMA_U32  Data1;
    tWMA_U16 Data2;
    tWMA_U16 Data3;
    tWMA_U8  Data4[8];
} GUID;

#endif // !GUID_DEFINED
//BYTE70CHANGE
typedef struct _HeaderInfo{
QWORD		h_Packets;
QWORD		h_PlayDuration;
XDAS_UInt32	h_MaxPacketSize;
GUID		h_StreamType;
XDAS_UInt32	h_cbTypeSpecific;
XDAS_UInt16	h_wStreamNum;
XDAS_UInt16	h_wFormatTag;
XDAS_UInt32 h_nSamplePerSec;
XDAS_UInt32 h_nAvgBytesPerSec;
XDAS_UInt16 h_nBlockAlign;
XDAS_UInt16 h_nChannel;
XDAS_UInt16 h_wValidBitsPerSample;
XDAS_UInt16 h_cbSizeWaveHeader;
XDAS_UInt32 h_dwChannelMask;
XDAS_UInt16 h_EncodeOptV;
XDAS_UInt32 h_SamplePerBlock;
}HeaderInfo;


#define DESC_NAME_MAX_LENGTH   64
typedef struct _MarkerEntry {
    QWORD   m_qOffset;
    QWORD   m_qtime;
    XDAS_UInt16    m_wEntryLen;
    XDAS_UInt32   m_dwSendTime;
    XDAS_UInt32   m_dwFlags;
    XDAS_UInt32   m_dwDescLen;
    XDAS_UInt16   m_pwDescName[DESC_NAME_MAX_LENGTH];
} MarkerEntry;

typedef struct _ECD_DESCRIPTOR {
    XDAS_UInt16         cbName;
    XDAS_UInt16     *pwszName;
    XDAS_UInt16         data_type;
    XDAS_UInt16         cbValue;
    union {
        XDAS_UInt16 *pwszString;
        XDAS_UInt8 *pbBinary;
        XDAS_Bool *pfBool;
        XDAS_UInt32 *pdwDword;
        tWMA_U64 *pqwQword;
        XDAS_UInt16  *pwWord;
    } uValue;
} ECD_DESCRIPTOR;

typedef struct tagWMAExtendedContentDescription
{
    XDAS_UInt16 cDescriptors;             // number of descriptors
    ECD_DESCRIPTOR *pDescriptors;  // pointer to all the descriptors
} tWMAExtendedContentDesc;

/* status */
#ifndef _WMAFILESTATUS_DEFINED
#define _WMAFILESTATUS_DEFINED
typedef enum tagWMAFileStatus
{
    cWMA_NoErr,                 /* -> always first entry */
                                /* remaining entry order is not guaranteed */
    cWMA_Failed,
    cWMA_BadArgument,
    cWMA_BadAsfHeader,
    cWMA_BadPacketHeader,
    cWMA_BrokenFrame,
    cWMA_NoMoreFrames,
    cWMA_BadSamplingRate,
    cWMA_BadNumberOfChannels,
    cWMA_BadVersionNumber,
    cWMA_BadWeightingMode,
    cWMA_BadPacketization,
    cWMA_BadDRMType,
    cWMA_DRMFailed,
    cWMA_DRMUnsupported,
    cWMA_DemoExpired,
    cWMA_BadState,
    cWMA_Internal,               /* really bad */
    cWMA_NoMoreDataThisTime,
	cWMA_HandleNotCreated		/*	If WMAcreate fails */
} tWMAFileStatus;
#endif /* _WMAFILESTATUS_DEFINED */

/* ...........................................................................
 *
 * Structures
 * ==========
 */

/* header */
#ifndef _WMAFILEHEADER_DEFINED
#define _WMAFILEHEADER_DEFINED
typedef PACKED struct tagWMAFileHeader
{
    XDAS_UInt16 version;				/*version of the codec*/
    XDAS_UInt32 sample_rate;			/*sampling rate*/
    XDAS_UInt16 num_channels;			/*number of audio channels*/
    XDAS_UInt32 duration;               /*of the file in milliseconds*/
    XDAS_UInt32 packet_size;            /*size of an ASF packet*/
    XDAS_UInt32 first_packet_offset;    /*byte offset to the first ASF packet*/
    XDAS_UInt32 last_packet_offset;     /*byte offset to the last ASF packet*/
    XDAS_UInt32 has_DRM;                /*does it have DRM encryption?*/
    XDAS_UInt32 LicenseLength;          /*License Length in the header*/
    XDAS_UInt32 bitrate;                /*bit-rate of the WMA bitstream*/
    // Added in V9
    XDAS_UInt16 pcm_format_tag;         /*wFormatTag in pcm header*/
    XDAS_UInt16 bits_per_sample;        /*number of bits per sample of mono
										   data (container size, always
										   multiple of 8)*/
    XDAS_UInt16 valid_bits_per_sample;  /*actual valid bits per sample of
										   mono data (less than or equal to
										   bits_per_sample)*/
    XDAS_UInt16 subformat_data1;        /*GUID information*/
    XDAS_UInt16 subformat_data2;        /*GUID information*/
    XDAS_UInt16 subformat_data3;        /*GUID information*/
    XDAS_UInt8  subformat_data4[8];     /*GUID information*/
    XDAS_UInt32 channel_mask;           /*which channels are present in stream*/

    /* HongCho: what else? */

} tWMAFileHeader;
#endif /* _WMAFILEHEADER_DEFINED */

/* content description */
#ifndef _WMAFILECONTDESC_DEFINED
#define _WMAFILECONTDESC_DEFINED
typedef PACKED struct tagWMAFileContDesc
{

    XDAS_UInt16 title_len;
    XDAS_UInt16 author_len;
    XDAS_UInt16 copyright_len;
    XDAS_UInt16 description_len;   /* rarely used */
    XDAS_UInt16 rating_len;        /* rarely used */

    XDAS_UInt8 *pTitle;
    XDAS_UInt8 *pAuthor;
    XDAS_UInt8 *pCopyright;
    XDAS_UInt8 *pDescription;
    XDAS_UInt8 *pRating;

} tWMAFileContDesc;
#endif /* _WMAFILECONTDESC_DEFINED */

typedef void * tHWMAFileState;

#ifdef DRM_SUPPORTED
/* license params */
#ifndef _WMAFILELICPARAMS_DEFINED
#define _WMAFILELICPARAMS_DEFINED
typedef PACKED struct tagWMAFileLicParams
{
    XDAS_UInt8 *pPMID;       /* portable media id */
    XDAS_UInt32 cbPMID;            /* length of the pPMID buffer */
} tWMAFileLicParams;
#endif /* _WMAFILELICPARAMS_DEFINED */
/* Date Params */
#ifndef _WMADATEPARAMS_DEFINED
#define _WMADATEPARAMS_DEFINED
typedef PACKED struct tagWMADateParams
{
    XDAS_UInt16 year;
    XDAS_UInt8 month;
    XDAS_UInt8 day;
} tWMADateParams;
#endif /* _WMADATEPARAMS_DEFINED */
#endif//DRM_SUPPORTED

typedef XDAS_UInt32 (*tpWMAFileCBGetData) (XDAS_Void* hDecode,
										   XDAS_UInt32 offset,
								           XDAS_UInt32 num_bytes,
										   XDAS_UInt8 **ppData);

/*===========================================================================*/
/*!  Description    : This handle is used to reference  WMA decoder
                      instance objects                                      !*/
/*---------------------------------------------------------------------------*/
typedef struct IWMA_Obj *IWMA_Handle;

/*===========================================================================*/
/*!  Description    : This structure must be the first field of all WMA
                    : instance objects                                      !*/
/*---------------------------------------------------------------------------*/
typedef struct IWMA_Obj {
    struct IWMA_Fxns *fxns;

} IWMA_Obj;

/*
// ===========================================================================
// IWMA_Cmd
//
// The Cmd enumeration defines the control commands for the WMA decoder
// control method.
*/
typedef IAUDDEC_Cmd IWMA_Cmd;
/*
// ===========================================================================
// control method commands
*/
typedef enum {GETHEADER =6, GETCONTENTDESCRIPTOR, GETEXGETCONTENTDESCRIPTOR,
				GETMARKER,PUTPARENT,GETPARENT,GETPCM}
					WMA_CMD_ID;

#define IWMA_GETSTATUS    XDM_GETSTATUS
#define IWMA_SETPARAMS    XDM_SETPARAMS
#define IWMA_RESET        XDM_RESET
#define IWMA_FLUSH        XDM_FLUSH
#define IWMA_SETDEFAULT   XDM_SETDEFAULT
#define IWMA_GETBUFINFO   XDM_GETBUFINFO
/*WMADecoder specific commands*/
#define IWMA_GETHEADER        			GETHEADER
#define IWMA_GETCONTENTDESCRIPTOR   	GETCONTENTDESCRIPTOR
#define IWMA_GETEXCONTENTDESCRIPTOR   	GETEXGETCONTENTDESCRIPTOR
#define IWMA_GETMARKER         			GETMARKER
#define IWMA_PUTPARENT        			PUTPARENT
#define IWMA_GETPARENT         			GETPARENT
#define IWMA_GETPCM         			GETPCM

// ===========================================================================
// IWMA_Status
//
// Status structure defines the parameters that can be changed or read
// during real-time operation of the alogrithm.

typedef struct IWMA_Status {
    IAUDDEC_Status  auddec_status; /* must be followed for audio decoder */
    tWMAFileHeader hdr;/*ASF header information*/
    tWMAFileContDesc *pCDesc;/*Pointer to content descriptor*/
    tWMAExtendedContentDesc *pECDesc;/*Pointer to extended content descriptor*/
    XDAS_Int32 NumberOfMarkers;/*Number of markers in the stream*/
    MarkerEntry *pEntry;/*Marker information */
    XDAS_Void *parent;/* Parent information for PUTPARENT cmd*/

} IWMA_Status;

/*===========================================================================*/
/*!  Description    : This structure defines the creation parameters for all
                    : WMA objects                                           !*/
/*---------------------------------------------------------------------------*/
typedef struct  IWMA_Params {
    IAUDDEC_Params auddec_params;
    XDAS_UInt32	nGetPCMinsideFlag;/*Flag to indicate if library outputs the
									samples*/
	tpWMAFileCBGetData	pWMAFileCBGetData;/* Pointer to callback function*/
	HeaderInfo hI;
	tWMA_U32 OffsetConsumedRCA;/*Bytes consumed by decoder from RCA buffer*/
	tWMA_U32 OffsetAddRCA; /*Total bytes in the RCA buffer*/
} IWMA_Params;

extern IWMA_Params IWMA_PARAMS;

/*===========================================================================*/
/*!  Description    : This structure defines the runtime parameters for all
                    : WMA objects                                           !*/
/*---------------------------------------------------------------------------*/
typedef struct  IWMA_DynamicParams {
    IAUDDEC_DynamicParams auddec_dynamicparams; /* must be second field of all
												   params structures */
    XDAS_Void *parent;/*parent info for GETPARENT cmd*/
    XDAS_Int32 *OutputBufPtr;/*Pointer to output buffer*/
    XDAS_Int32 OutputBufSize;/*Size of output buffer*/
    XDAS_Int32 OutputSamplesRequested;/*Number of samples requested with GETPCM
										cmd*/
} IWMA_DynamicParams;

extern IWMA_DynamicParams IWMA_DYPARAMS;

/*===========================================================================*/
/*!  Description    : This structure defines the input parameters for all
                    : WMA objects                                           !*/
/*---------------------------------------------------------------------------*/
typedef struct  IWMA_InArgs {
    IAUDDEC_InArgs auddec_inArgs;/*must be second field of all params
								   structures */
   	XDAS_UInt32 TotalConsumedRCA;
} IWMA_InArgs;

extern IWMA_InArgs IWMA_INARGS ;

/*===========================================================================*/
/*!  Description    : This structure defines the input parameters for all
                    : WMA objects                                           !*/
/*---------------------------------------------------------------------------*/
typedef struct  IWMA_OutArgs {
    IAUDDEC_OutArgs auddec_outArgs; /* must be second field of all params
									   structures */
    XDAS_Int32 OutputSamplesReady;/*Number of samples decoded*/
} IWMA_OutArgs;

extern IWMA_OutArgs IWMA_OUTARGS ;
/*===========================================================================*/
/*!  Description    : This structure defines all of the operations on WMA
                    : objects                                               !*/
/*---------------------------------------------------------------------------*/
typedef struct IWMA_Fxns {
    IAUDDEC_Fxns iauddec; /* IWMA extends IAUDDEC */
} IWMA_Fxns;

#endif  /* IWMA_ */
