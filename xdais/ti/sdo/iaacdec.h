/*
 *  ======== iaacdecoder.h ========
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the DECODE algorithm.
 */
#ifndef IAACDECDECODER_DECODE_
#define IAACDECDECODER_DECODE_

#include <ti/xdais/ialg.h>
#include <ti/xdais/dm/iauddec.h>

/*
 *  ======== IDECODE_Obj ========
 *  This structure must be the first field of all DECODE instance objects.
 */
typedef struct IAACDEC_Obj {
    struct IAACDEC_Fxns *fxns;
} IAACDEC_Obj;

/*
 *  ======== IAACDEC_Handle ========
 *  This handle is used to reference all DECODE instance objects.
 */
typedef struct IAACDEC_Obj  *IAACDEC_Handle;

/*
 *  ======== IAACDEC_Status ========
 *  This structure defines the parameters that can be changed at runtime
 *  (read/write), and the instance status parameters (read-only).
 */
typedef struct IAACDEC_Status{
    IAUDDEC_Status  auddec_status; /* must be followed for audio decoder */
	XDAS_Int32 isValid;
#ifdef AAC_RAW_DATA_FLAG
    XDAS_Int32 ulSamplingRateIdx;
    XDAS_Int32 nProfile;
#endif
}IAACDEC_Status;

/*
// ===========================================================================
// IAACDEC_Cmd
//
// The Cmd enumeration defines the control commands for the AACDEC
// control method.
*/
typedef IAUDDEC_Cmd IAACDEC_Cmd;


/*
// ===========================================================================
// control method commands
*/
#define IAACDEC_GETSTATUS  XDM_GETSTATUS
#define IAACDEC_SETPARAMS  XDM_SETPARAMS
#define IAACDEC_RESET      XDM_RESET
#define IAACDEC_SETDEFAULT XDM_SETDEFAULT
#define IAACDEC_FLUSH      XDM_FLUSH
#define IAACDEC_GETBUFINFO XDM_GETBUFINFO

/*
// ===========================================================================
// IAACDEC_Params
//
// This structure defines the creation parameters for all AACDEC objects
*/
typedef struct IAACDEC_Params {
    IAUDDEC_Params auddec_params; /* must be second element of creation params */
} IAACDEC_Params;

/*
// ===========================================================================
// IAACDEC_PARAMS
//
// Default parameter values for AACDEC instance objects
*/
extern IAACDEC_Params IAACDEC_PARAMS;


/*
// ===========================================================================
// IAACDEC_Params
//
// This structure defines the run time parameters for all AACDEC objects
*/
typedef struct IAACDEC_DynamicParams {
    IAUDDEC_DynamicParams auddec_dynamicparams; /* must be second field of all params structures */
	Int DownSampleSbr;
} IAACDEC_DynamicParams;


typedef struct IAACDEC_InArgs {
    IAUDDEC_InArgs auddec_inArgs; /* must be second field of all params structures */
} IAACDEC_InArgs;


typedef struct IAACDEC_OutArgs{
    IAUDDEC_OutArgs  auddec_outArgs; /* must be followed for audio decoder */
}IAACDEC_OutArgs;

/*
 *  ======== IAACDEC_Fxns ========
 *  This structure defines all of the operations on DECODE objects.
 */
typedef struct IAACDEC_Fxns{
	IAUDDEC_Fxns iauddec; /* must be second element of objects */
}IAACDEC_Fxns;

/*
 *  ======== AAC_TII_IAACDEC ========
 *  This structure defines TI's implementation of the IDECODE  interface
 *  for the AAC_TNI module.
 */
extern IAACDEC_Fxns AACDEC_TII_IAACDEC;

extern const char AACDEC_TNI_AACDECDate[];
extern const char AACDEC_TNI_AACDECLibVer[];
extern const char AACDEC_TNI_AACDECCopyright[];

#endif	/* IDECODE_ */



